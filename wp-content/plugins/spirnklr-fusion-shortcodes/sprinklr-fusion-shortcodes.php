<?php
/*
  Plugin Name: Sprinklr Fusion core Customization
  Plugin URI: http://sprinklr.com
  Description: Sprinklr Fusion core Customization for shortcodes
  Version: 1.0
  Author: Muhammad Irfan - PressTigers
  Author URI: http://presstiger.com
  Text Domain: sprinklr
 */
function sprinklr_custom_shortcodes_activate() {

    // Activation code here...
}
register_activation_hook( __FILE__, 'sprinklr_custom_shortcodes_activate' );

function sprinklr_plugin_set_priority() {
   
    // ensure path to this file is via main wp plugin path
    $sprinklr_wp_path_to_this_file = preg_replace('/(.*)plugins\/(.*)$/', WP_PLUGIN_DIR."/$2", __FILE__);
    $sprinklr_this_plugin = plugin_basename(trim($sprinklr_wp_path_to_this_file));
    $sprinklr_active_plugins = get_option('active_plugins');
    
    $sprinklr_this_plugin_key = array_search($sprinklr_this_plugin, $sprinklr_active_plugins);
    $sprinklr_current_item = $sprinklr_active_plugins[$sprinklr_this_plugin_key];
    unset($sprinklr_active_plugins[$sprinklr_this_plugin_key]);
    array_push($sprinklr_active_plugins, $sprinklr_current_item);
    update_option('active_plugins', $sprinklr_active_plugins);
    
}
add_action("plugins_loaded", "sprinklr_plugin_set_priority");

if( ! class_exists( 'Sprinklr_FusionCore_Plugin') && class_exists('FusionCore_Plugin') ) {
    class Sprinklr_FusionCore_Plugin extends FusionCore_Plugin {
        /**
         * Plugin version, used for cache-busting of style and script file references.
         *
         * @since   1.0.0
         *
         * @var  string
         */
        const VERSION = '1.7.4';
        
        /**
         * Instance of this class.
         *
         * @since   1.0.0
         *
         * @var   object
         */
        protected static $instance = null;
        
        /**
         * Initialize the plugin by setting localization and loading public scripts
         * and styles.
         *
         * @since    1.0.0
         */
          function __construct() {
            define('SPRINKLR_SHORTCODES_URI', plugin_dir_url( __FILE__ ) . 'includes');
            define('SPRINKLR_SHORTCODES_DIR', plugin_dir_path( __FILE__ ) .'includes');
            remove_action('wp_ajax_fusion_shortcodes_popup', array('FusionCore_Plugin', 'popup'), 10);
            add_action('wp_ajax_fusion_shortcodes_popup', array(&$this, 'popup'), 10);
        }
        
        /**
         * Popup function which will show shortcode options in thickbox.
         *
         * @return void
         */
        function popup() {


            require_once( SPRINKLR_SHORTCODES_DIR . '/fusion-sc.php' );

            die();

        }
    }
    new Sprinklr_FusionCore_Plugin();
 }

 if(!function_exists('sprinklr_content_strip_br')){
    function sprinklr_content_strip_br($content){
        $Old     = array( '<br />', '<br>' );
        $New     = array( '','' );
        $content = str_replace( $Old, $New, $content );
        return $content;
    }
 }
require_once( 'shortcodes/hubspot.php' );
require_once( 'shortcodes/blog.php' );
require_once( 'shortcodes/shortcodes.php' );
require_once( 'shortcodes/services.php' );
require_once( 'shortcodes/class-heading.php' );
require_once( 'shortcodes/spr-custom-images-content.php' );



