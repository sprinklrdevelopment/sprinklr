<?php

// loads the shortcodes class, wordpress is loaded with it
require_once( FUSION_TINYMCE_DIR.'/shortcodes.class.php' );
require_once( 'shortcodes.class.php' );
require_once( 'config.php' );

// get popup type
$popup = trim( $_GET['popup'] );
$shortcode = new sprinklr_fusion_shortcodes( $popup );

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<body>
<script>
jQuery(document).ready(function($) {
	$ = jQuery;
    // update upload button ID
    $( '#form-child-add' ).click(function() {

		jQuery('.fusion-upload-button').each(function() {
			var rand_no = Math.floor(Math.random() * 600666) + 1;
			var prev_upload_id = jQuery(this).data('upid');
			var new_upload_id = prev_upload_id + 1+rand_no;
			jQuery(this).attr('data-upid', new_upload_id);
		});
	});
    
});


jQuery(document).ready(function($) {

	// activate upload button
	jQuery('.fusion-upload-button').live('click', function(e) {
		//e.preventDefault();
		var rand_no = Math.floor(Math.random() * 600666) + 1;
		var upid_val = $(this).attr('data-upid');
		var upid = upid_val + 1+rand_no;
		jQuery(this).attr('data-upid', upid);

		if($(this).hasClass('remove-image')) {
			$('.fusion-upload-button[data-upid="' + upid + '"]').parent().find('img').attr('src', '').hide();
			$('.fusion-upload-button[data-upid="' + upid + '"]').parent().find('input').attr('value', '');
			$('.fusion-upload-button[data-upid="' + upid + '"]').text('Upload').removeClass('remove-image');

			return;
		}

		var file_frame = wp.media.frames.file_frame = wp.media({
			title: 'Select Image',
			button: {
				text: 'Select Image',
			},
			frame: 'post',
			multiple: false  // Set to true to allow multiple files to be selected
		});

		file_frame.open();

		$('.media-menu a:contains(Insert from URL)').remove();

		file_frame.on( 'select', function() {
			var selection = file_frame.state().get('selection');
			selection.map( function( attachment ) {
				attachment = attachment.toJSON();

				$('.fusion-upload-button[data-upid="' + upid + '"]').parent().find('img').attr('src', attachment.url).show();
				$('.fusion-upload-button[data-upid="' + upid + '"]').parent().find('input').attr('value', attachment.url);

				themefusion_shortcodes.loadVals();
				themefusion_shortcodes.cLoadVals();
			});

			$('.fusion-upload-button[data-upid="' + upid + '"]').text('Remove').addClass('remove-image');
			$('.media-modal-close').trigger('click');
		});

		file_frame.on( 'insert', function() {
			var selection = file_frame.state().get('selection');
			var size = jQuery('.attachment-display-settings .size').val();

			selection.map( function( attachment ) {
				attachment = attachment.toJSON();

				if(!size) {
					attachment.url = attachment.url;
				} else {
					attachment.url = attachment.sizes[size].url;
				}

				$('.fusion-upload-button[data-upid="' + upid + '"]').parent().find('img').attr('src', attachment.url).show();
				$('.fusion-upload-button[data-upid="' + upid + '"]').parent().find('input').attr('value', attachment.url);

				themefusion_shortcodes.loadVals();
				themefusion_shortcodes.cLoadVals();
			});

			$('.fusion-upload-button[data-upid="' + upid + '"]').text('Remove').addClass('remove-image');
			$('.media-modal-close').trigger('click');
		});
		return false;
	});

    var ww = jQuery('#post_id_reference').text();
    window.original_send_to_editor = window.send_to_editor;
    window.send_to_editor_clone = function(html){
            imgurl = jQuery('a','<p>'+html+'</p>').attr('href');
            jQuery('#'+formfield).val(imgurl);
            tb_remove();
    }
    jQuery('input.uploadfile').click(function() {
            window.send_to_editor=window.send_to_editor_clone;
            formfield = jQuery(this).attr('name');
            tb_show('', 'media-upload.php?post_id=' + ww + '&type=image&TB_iframe=true');
            return false;
    });
});
</script>
<div id="fusion-popup">

	<div id="fusion-shortcode-wrap">

		<div id="fusion-sc-form-wrap">

			<?php
			$select_shortcode = array(
					'select' => 'Choose a Shortcode',
					'alert' => 'Alert',
					'blog' => 'Blog',
					'button' => 'Button',
					'checklist' => 'Checklist',
					//'clientslider' => 'Client Slider',
					//'code'		=> 'Code Block',
					'columns' => 'Columns',
					'contentboxes' => 'Content Boxes',
					'countersbox' => 'Counters Box',					
					'counterscircle' => 'Counters Circle',
					'dropcap' => 'Dropcap',
					'flipboxes' => 'Flip Boxes',
					//'fontawesome' => 'Font Awesome',
					'fusionslider' => 'Fusion Slider',
					'fullwidth' => 'Full Width Container',
					'googlemap' => 'Google Map',
					'highlight' => 'Highlight',
					
					'imageframe' => 'Image Frame',
					'lightbox' => 'Lightbox',
					'menuanchor' => 'Menu Anchor',
					'modal' => 'Modal',
					'modaltextlink' => 'Modal Text Link',
					'onepagetextlink' => 'One Page Text Link',
					'person' => 'Person',
					'popover' => 'Popover',
					'postslider' => 'Post Slider',
					'pricingtable' => 'Pricing Table',
					'progressbar' => 'Progress Bar',
					'recentposts' => 'Recent Posts',
					'recentworks' => 'Recent Works',
                                        'heading' => 'Sprinklr Heading',
                                        'spr_content_button' => ' Sprinklr Home (Experience Cloud Section)',
                                        'spr_benchmark' => 'Sprinklr Home (Benchmark Section)',
					'hubspot' => 'Sprinklr Hubspot Form',
                                        'spr_content_images' => 'Sprinklr Experience Cloud (Social Experience Core Section)',
                                        'services' => ' Sprinklr Experience Cloud (Premium Modules Section)',
                                        'spr_image_tabs' => 'Social Experience Core (Sprinklr Tabs)',
					//'spr_carousel' => 'Sprinklr Image Carousel',
					'spr_tweets' => 'Sprinklr Tweets',
					'sectionseparator' => 'Section Separator',
					'separator' => 'Separator',
					'sharingbox' => 'Sharing Box',
					'slider' => 'Slider',
					'sociallinks' => 'Social Links',
					'soundcloud' => 'SoundCloud',
					'table' => 'Table',
					'tabs' => 'Tabs',
					'taglinebox' => 'Tagline Box',
					'testimonials' => 'Testimonials',
					'title' => 'Title',
					'toggles' => 'Toggles',
					'tooltip' => 'Tooltip',
					'vimeo' => 'Vimeo',
					'woofeatured' => 'Woocommerce Featured Products Slider',
					'wooproducts' => 'Woocommerce Products Slider',
					'youtube' => 'Youtube'
			);
			?>
			<table id="fusion-sc-form-table" class="fusion-shortcode-selector">
				<tbody>
					<tr class="form-row">
						<td class="label">Choose Shortcode</td>
						<td class="field">
							<div class="fusion-form-select-field">
							<div class="fusion-shortcodes-arrow">&#xf107;</div>
								<select name="fusion_select_shortcode" id="fusion_select_shortcode" class="fusion-form-select fusion-input">
									<?php foreach($select_shortcode as $shortcode_key => $shortcode_value): ?>
									<?php if($shortcode_key == $popup): $selected = 'selected="selected"'; else: $selected = ''; endif; ?>
									<option value="<?php echo $shortcode_key; ?>" <?php echo $selected; ?>><?php echo $shortcode_value; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<form method="post" id="fusion-sc-form">

				<table id="fusion-sc-form-table">

					<?php echo $shortcode->output; ?>

					<tbody class="fusion-sc-form-button">
						<tr class="form-row">
							<td class="field"><a href="#" class="fusion-insert">Insert Shortcode</a></td>
						</tr>
					</tbody>

				</table>
				<!-- /#fusion-sc-form-table -->

			</form>
			<!-- /#fusion-sc-form -->

		</div>
		<!-- /#fusion-sc-form-wrap -->

		<div class="clear"></div>

	</div>
	<!-- /#fusion-shortcode-wrap -->

</div>
<!-- /#fusion-popup -->

</body>
</html>