<?php

/**
 *
 * Shortcode for Benchmark
 * @param array $atts
 * @return string HTML
 * Usage:
 * [spr_benchmark title="" heading_text="" button_text="" button_link="" textcolor="" icon="" class="" id=""]Content[/spr_benchmark]
 *
 */
if (!function_exists('sprinklr_benchmark_shortcode_render')) {

    function sprinklr_benchmark_shortcode_render($atts, $content = null) {
        extract(shortcode_atts(array(
            'class' => '',
            'id' => '',
            'heading_text' => '',
            'icon' => 'fa-align-left',
            'title' => '',
            'button_text' => '',
            'button_link' => '',
            'textcolor' => '#fff',
                        ), $atts));
        $spr_attr_id = '';
        if (!empty($id)) {
            $spr_attr_id = 'id="' . $id . '"';
        }
        $spr_style = '';
        if (isset($textcolor) && !empty($textcolor)) {
            $spr_style .= 'color: ' . $textcolor . ';';
        }
        $spr_style_attr = '';
        if (!empty($spr_style)) {
            $spr_style_attr = 'style="' . $spr_style . '"';
        }
        $return_string = '<div class="benchmark-content col-sm-5 col-xs-12 ' . $class . '" ' . $spr_attr_id . ' ' . $spr_style_attr . ' >';
        if (isset($heading_text) && !empty($heading_text)) {
            $return_string .= '<p>' . $heading_text . '</p>';
        }
        $return_string .= '<h3>';
        if (isset($title) && !empty($icon)) {
            $return_string .= '<i class="fa ' . $icon . '"></i> ';
        }
        if (isset($title) && !empty($title)) {
            $return_string .= $title;
        }

        $return_string .= '</h3>';
        if (isset($content) && !empty($content)) {
            $return_string .= '<p>' . $content . '</p>';
        }
        if (isset($button_text) && !empty($button_text)) {
            $return_string .= '<a href="' . $button_link . '" class="btn btn-default">' . $button_text . '</a>';
        }
        $return_string .= '
    </div>';
        return $return_string;
    }

    add_shortcode('spr_benchmark', 'sprinklr_benchmark_shortcode_render');
}

/**
 *
 * Shortcode for Button with shortcode
 * @param array $atts
 * @return string HTML
 * Usage:
 * [spr_content_button title="" image="" button_text="" button_link="" textcolor="" class="" id=""]Content[/spr_content_button]
 *
 */
if (!function_exists('sprinklr_content_button_shortcode_render')) {

    function sprinklr_content_button_shortcode_render($atts, $content = null) {
        extract(shortcode_atts(array(
            'class' => '',
            'id' => '',
            'title' => '',
            'image' => '',
            'button_text' => '',
            'button_link' => '',
            'textcolor' => '#fff',
                        ), $atts));
        $spr_attr_id = '';
        if (!empty($id)) {
            $spr_attr_id = 'id="' . $id . '"';
        }
        $spr_style = '';
        if (isset($textcolor) && !empty($textcolor)) {
            $spr_style .= 'color: ' . $textcolor . ';';
        }
        $spr_style_attr = '';
        if (!empty($spr_style)) {
            $spr_style_attr = 'style="' . $spr_style . '"';
        }
        $return_string = '<div class="experience-cloud-content text-center ' . $class . '" ' . $spr_attr_id . ' ' . $spr_style_attr . ' >';

        if (isset($image) && !empty($image)) {
            $return_string .= '<img class="spr-aligncenter size-full" alt="' . sanitize_title($title) . '" src="' . $image . '" width="51" height="51">';
        }
        if (isset($title) && !empty($title)) {
            $return_string .= '<h2>' . $title . '</h2>';
        }
        if (isset($content) && !empty($content)) {
            $return_string .= '<p>' . $content . '</p>';
        }
        if (isset($button_text) && !empty($button_text)) {
            $return_string .= '<a href="' . $button_link . '" class="btn btn-default spr-bg-hover orange-hover">' . $button_text . '</a>';
        }
        $return_string .= '</div>';
        return $return_string;
    }

    add_shortcode('spr_content_button', 'sprinklr_content_button_shortcode_render');
}

/**
 *
 * Shortcode for Images tabs Shortcode
 * @param array $atts
 * @return string HTML
 * Usage:
 *
 */
if (!function_exists('sprinklr_image_tabs_render')) {

    function sprinklr_image_tabs_render($atts, $content = null) {
        extract(shortcode_atts(array(
            'class' => '',
            'id' => '',
                        ), $atts));
        global $tabs, $tabs_count;
        $tabs_count = 0;
        $tabs = array();
        $spr_attr_id = '';
        if (!empty($id)) {
            $spr_attr_id = 'id="' . $id . '"';
        }
        if (isset($content) && !empty($content)) {
            if (function_exists('sprinklr_content_strip_br')) {
                $content = sprinklr_content_strip_br($content);
            }
            do_shortcode($content);
        }
        $return_string = '';
        $return_string .= '<div class="social-expeirence text-center ' . $class . '" ' . $spr_attr_id . '>';
        $tabs_rand = rand(52, 9666);
        if (isset($tabs) && is_array($tabs) && count($tabs) > 0) {
            $return_string .= '<ul class="nav nav-tabs social-experience-list">';
            foreach ($tabs as $key => $tab) {
                $remove_space_from_tab = str_replace(' ', '-', $tab['name']);
                $return_string .= '<li>';
                $return_string .= '<a data-toggle="tab" href="#' . $remove_space_from_tab . '-' . $tabs_rand . '-' . $key . '">';
                if (isset($tab['section_top_images']) && !empty($tab['section_top_images'])) {
                    $return_string .= '<span><img src="' . $tab['section_top_images'] . '" alt="" /></span>';
                }
                if (isset($tab['name']) && !empty($tab['name'])) {
                    $return_string .= '<small>' . $tab['name'] . '</small>';
                }
                $return_string .= '</a>';
                $return_string .= '</li>';
            }

            $return_string .= '</ul>';
            $i = 0;
            $return_string .= '<div class="tab-content">';
            foreach ($tabs as $key => $tab) {
                $active_class = '';
                if ($i == 0) {
                    $active_class = 'in active';
                }
                $i++;
                $remove_space_from_tab = str_replace(' ', '-', $tab['name']);
                $return_string .= '<div id="' . $remove_space_from_tab . '-' . $tabs_rand . '-' . $key . '" class="tab-pane fade ' . $active_class . '">';
                $return_string .= '<div class="col-sm-6 text-left">';
                $return_string .= '<h2>' . $tab['name'] . '</h2>';
                $return_string .= '<strong>' . $tab['sub_heading'] . '</strong>';
                $return_string .= html_entity_decode($tab['tab_content']);
                $return_string .= '<a href="' . $tab['link'] . '" target="' . $tab['target'] . '" class="btn btn-default exp-core-btn">' . $tab['button_text'] . '</a>';
                $return_string .= '</div>';
                $return_string .= '<div class="col-sm-6"><img src="' . $tab['content_image'] . '" alt="" /></div>';
                $return_string .= '</div>';
            }

            $return_string .= '</div>';
        }
        $return_string .= '</div>';
        return $return_string;
    }

    add_shortcode('spr_image_tabs', 'sprinklr_image_tabs_render');
}

/**
 *
 * Shortcode for Images tab Shortcode
 * @param array $atts
 * @return string HTML
 * Usage:
 *
 */
if (!function_exists('sprinklr_image_tabs_child_render')) {

    function sprinklr_image_tabs_child_render($atts, $content = null) {
        extract(shortcode_atts(array(
            'name' => '',
            'section_top_images' => '',
            'sub_heading' => '',
            'content_image' => '',
            'button_text' => '',
            'link' => '#',
            'target' => '_self',
                        ), $atts));
        global $tabs, $tabs_count;
        if (isset($tabs) && !is_array($tabs)) {
            $tabs = array();
        }
        $tabs[] = array('name' => $name, 'sub_heading' => $sub_heading, 'section_top_images' => $section_top_images, 'content_image' => $content_image, 'button_text' => $button_text, 'link' => $link, 'target' => $target, 'tab_content' => $content);
        $tabs_count++;
        //return $tabs;
    }

    add_shortcode('spr_image_tab', 'sprinklr_image_tabs_child_render');
}

/**
 *
 * Shortcode for Images Carousel
 * @param array $atts
 * @return string HTML
 * Usage:
 *
 */
if (!function_exists('sprinklr_carousel_shortcode_function')) {

    function sprinklr_carousel_shortcode_function($atts, $content = null) {
        extract(shortcode_atts(array(
            'class' => '',
            'id' => '',
            'title' => '',
                        ), $atts));

        $spr_attr_id = '';
        if (!empty($id)) {
            $spr_attr_id = 'id="' . $id . '"';
        }

        $spr_style = '';
        if (isset($textcolor) && !empty($textcolor)) {
            $spr_style .= 'color: ' . $textcolor . ';';
        }
        $spr_style_attr = '';
        if (!empty($spr_style)) {
            $spr_style_attr = 'style="' . $spr_style . '"';
        }
        $return_string = '<div class="spr-content-section ' . $class . '" ' . $spr_attr_id . ' ' . $spr_style_attr . ' >';

        $return_string .= '<div class="spr-content">';
        if (isset($title) && !empty($title)) {
            $return_string .= '<p>' . $title . '</p>';
        }

        $return_string .= '<div class="spr-content-images">';
        if (isset($content) && !empty($content)) {
            if (function_exists('sprinklr_content_strip_br')) {
                $content = sprinklr_content_strip_br($content);
            }
            $return_string .= do_shortcode($content);
        }
        $return_string .= '</div>';

        $return_string .= '
   		</div>
    </div>';
        return $return_string;
    }

    add_shortcode('spr_carousel', 'sprinklr_carousel_shortcode_function');
}

/**
 *
 * Shortcode for Images Carousel child
 * @param array $atts
 * @return string HTML
 * Usage:
 *
 */
if (!function_exists('sprinklr_carousel_image_child_shortcode_function')) {

    function sprinklr_carousel_image_child_shortcode_function($atts, $content = null) {
        extract(shortcode_atts(array(
            'name' => '',
            'image' => '',
            'image_link' => '#',
                        ), $atts));
        $target_attr = '';
        if (!empty($target)) {
            $target_attr = 'class="' . $target . '"';
        }
        $image_item = '<div class="spr-image-item spr-bg-hover">';
        if (isset($image) && !empty($image)) {
            $image_item .= '<figure><a href="' . $image_link . '" ><img src="' . $image . '" alt="' . sanitize_title($name) . '" title="' . $name . '"></a></figure>';
        }
        $image_item .= '</div>';
        return $image_item;
    }

    add_shortcode('spr_carousel_image', 'sprinklr_carousel_image_child_shortcode_function');
}

// 
/**
 *
 * Shortcode for Tweets in editor
 * @param array $atts
 * @return string HTML
 * Usage:
 *
 */
if (!function_exists('sprinklr_tweets_shortcode_function')) {

    function sprinklr_tweets_shortcode_function($atts, $content = null) {
        extract(shortcode_atts(array(
            'class' => '',
            'id' => '',
        ), $atts));
        $spr_attr_id = '';
        if (!empty($id)) {
            $spr_attr_id = 'id="' . $id . '"';
        }
        $return_string = '<div class="spr-tweets-section ' . $class . '" ' . $spr_attr_id . ' >';
        $return_string .= '<div class="spr-content">';
        $return_string .= '<div class="spr-content-images">';
        if (isset($content) && !empty($content)) {
            if (function_exists('sprinklr_content_strip_br')) {
                $content = sprinklr_content_strip_br($content);
            }
            $return_string .= apply_filters('the_content', $content);
        }
        $return_string .= '</div>';
        $return_string .= '</div>
        </div>';
        return $return_string;
    }

    add_shortcode('spr_tweets', 'sprinklr_tweets_shortcode_function');
}