<?php
// Services Shortcode
function sprinklr_content_images_shortcode_function($atts, $content = null){
   extract(shortcode_atts(array(
      			'class'				=> '',
				'id'				=> '',
				'section_top_image' => '',
				'title'				=> '',
				'description'		=> '',
				'button_text' 		=> '',
				'button_link' 		=> '',
				'textcolor'			=> '#fff',
				'target'			=> '_blank',
   ), $atts));

	$spr_attr_id = '';
	if(!empty($id)){
		$spr_attr_id='id="'.$id.'"';
	}
	
	$spr_style = '';
	if(isset($textcolor) && !empty($textcolor)){
		$spr_style .= 'color: '.$textcolor.';';
	}
	$spr_style_attr='';
	if(!empty($spr_style)){
		$spr_style_attr='style="'.$spr_style.'"';
	}
	$return_string = '<div class="spr-content-section '.$class.'" '.$spr_attr_id.' '.$spr_style_attr.' >';
		if(isset($section_top_images) && !empty($section_top_images)){
   			$return_string .= '<div class="spr-top-image">
					   			<img class="spr-aligncenter size-full" alt="" src="'.$section_top_images.'">
					   		</div>';
		}
   		$return_string .= '<div class="spr-content">';
	   		if(isset($title) && !empty($title)){
	   			$return_string .= '<h2>'.$title.'</h2>';
	   		}
	   		if(isset($description) && !empty($description)){
	   			$return_string .= '<p>'.$description.'</p>';
	   		}
   			$return_string .= '<div class="spr-content-images">';
   			if(isset($content) && !empty($content)){
   				if(function_exists('sprinklr_content_strip_br')){
   					$content  = sprinklr_content_strip_br($content);
   				}
   				$return_string .= do_shortcode($content);
   			}
   			$return_string .= '</div>';
   			if(isset($button_text) && !empty($button_text)){
	   			$return_string .= '<div class="clear"></div><div class="btn transparent-btn"><a href="'.$button_link.'" target="'.$target.'" class="btn btn-transparent spr-bg-hover">'.$button_text.'</a></div>';
	   		}
   			$return_string .= '
   		</div>
    </div>';
   return $return_string;
}
// Heading Shortcode
function sprinklrspr_content_image_child_shortcode_function($atts, $content = null){
   extract(shortcode_atts(array(
      			'name'			=> '',
				'image'			=> '',
				'image_link'	=> '#',
   ), $atts));
   $target_attr= '';
	if(!empty($target)){
		$target_attr='class="'.$target.'"';
	}
   $image_item = '<div class="spr-image-item spr-bg-hover">';
	   if(isset($image) && !empty($image)){
	   		$image_item .= '<figure><a href="'.$image_link.'" ><img src="'.$image.'" alt="'.sanitize_title($name).'"></a></figure>';
	   }
	   if(isset($name) && !empty($name)){
	   		$image_item .= '<h4><a href="'.$image_link.'">'.$name.'</a></h4>';
	   }
   $image_item .= '</div>';
   return $image_item;
}

add_shortcode('spr_content_image', 'sprinklrspr_content_image_child_shortcode_function');
add_shortcode('spr_content_images', 'sprinklr_content_images_shortcode_function');
