<?php
// Services Shortcode
function sprinklr_custom_content_images_shortcode_function($atts, $content = null){
   extract(shortcode_atts(array(
      			'class'				=> '',
				'id'				=> '',
				'title'				=> '',
				'description'		=> '',
				'section_top_icon' => '',
				'textcolor'			=> '#fff',
   ), $atts));
	$services_attr_id = '';
	if(!empty($id)){
		$services_attr_id='id="'.$id.'"';
	}
	$services_style = '';
	if(isset($textcolor) && !empty($textcolor)){
		$services_style .= 'color: '.$textcolor.';';
	}
	$services_style_attr='';
	if(!empty($services_style)){
		$services_style_attr='style="'.$services_style.'"';
	}
	$return_string = '<div class="premium-features col-sm-10 text-center '.$class.'" '.$services_attr_id.' '.$services_style_attr.' >';
		if(isset($section_top_icon) && !empty($section_top_icon)){
   			$return_string .= '<span class="yellow-plus"><i class="fa '.$section_top_icon.'"></i></span>';
		}
   		if(isset($title) && !empty($title)){
   			$return_string .= '<h2>'.$title.'</h2>';
   		}
   		if(isset($description) && !empty($description)){
   			$return_string .= '<strong>'.$description.'</strong>';
   		}
		$return_string .= '<div class="clearfix"></div>';
		if(isset($content) && !empty($content)){
			$return_string .= ' <ul>';
			if(function_exists('spr_content_strip_br')){
				$content  = spr_content_strip_br($content);
			}
			$return_string .= do_shortcode($content);
			$return_string .= ' </ul>';
		}
   	$return_string .= '
    </div>';
   return $return_string;
}
// Heading Shortcode
function sprinklr_service_child_shortcode_function($atts, $content = null){
   extract(shortcode_atts(array(
      			'name'		=> '',
				'image'		=> '',
				'link'		=> '#',
				'target'	=> '_self',
   ), $atts));
   $service_item = '';
   if((isset($name) && !empty($name)) || (isset($image) && !empty($image))){
	   $target_attr= '';
		if(!empty($target)){
			$target_attr = 'class="'.$target.'"';
		}
	   $service_item .= ' <li><a href="'.$link.'" '.$target_attr.'>';
		   if(isset($image) && !empty($image)){
		   		$service_item .= '<img src="'.$image.'" alt="'.sanitize_title($name).'">';
		   }
		   $service_item .= '<h4>'.$name.'</h4>';
		   $service_item .= '<p>'.do_shortcode($content).'</p>';
		   $service_item .= '<div class="clearfix"></div>';
	   $service_item .= '</a></li>';
	}
   return $service_item;
}

add_shortcode('service', 'sprinklr_service_child_shortcode_function');
add_shortcode('services', 'sprinklr_custom_content_images_shortcode_function');