<?php
// Heading Shortcode
function sprinklr_heading_shortcode_function($atts, $content = null){
   extract(shortcode_atts(array(
      			'class'			=> '',
				'id'			=> '',
				'content_align'	=> 'center',
				'margin_top'	=> '',
				'margin_bottom'	=> '',
				'color' 		=> '',
				'font_size'		=> '',
                                'heading_border' => 'no',
				'line_height'	=> '',
				'size'			=> 1,
   ), $atts));
   $title_size = 'two';
   if( $size== '1' ) {
		$title_size = 'one';
	} else if( $size == '2' ) {
		$title_size = 'two';
	} else if( $size== '3' ) {
		$title_size = 'three';
	} else if( $size == '4' ) {
		$title_size = 'four';
	} else if( $size == '5' ) {
		$title_size = 'five';
	} else if( $size== '6' ) {
		$title_size = 'six';
	} else {
		$title_size = 'two';
	}
	$heading_style = '';
	if(isset($color) && !empty($color)){
		$heading_style .= 'color: '.$color.';';
	}
	if(isset($content_align) && !empty($content_align)){
		$heading_style .= 'text-align: '.$content_align.';';
	}
	if(isset($font_size) && !empty($font_size)){
		$heading_style .= 'font-size: '.$font_size.';';
	}
	if(isset($line_height) && !empty($line_height)){
		$heading_style .= 'line-height: '.$line_height.';';
	}
	if(isset($margin_top) && !empty($margin_top)){
		$heading_style .= 'margin-top: '.$margin_top.';';
	}
	if(isset($margin_bottom) && !empty($margin_bottom)){
		$heading_style .= 'margin-bottom: '.$margin_bottom.';';
	}
	$heading_style_attr='';
	if(!empty($heading_style)){
		$heading_style_attr='style="'.$heading_style.'"';
	}
	$heading_attr_id = '';
	if(!empty($id)){
		$heading_attr_id='id="'.$id.'"';
	}
	$heading_attr_class = '';
	if(!empty($class)){
		$heading_attr_class='class="'.$class.'"';
	}
        
        $heading_border_class = '';
	if(isset($heading_border) && $heading_border == 'yes'){
		$heading_border_class='spr-heading-border';
	}


   $return_string = '<div class="spr-heading '.$heading_border_class.' fusion-title-size-' . $title_size.'"><h'.$size.'  '.$heading_attr_id.' '.$heading_attr_class.' '.$heading_style_attr.'>'.do_shortcode($content).'</h'.$size.'></div>';

   return $return_string;
}
add_shortcode('heading', 'sprinklr_heading_shortcode_function');