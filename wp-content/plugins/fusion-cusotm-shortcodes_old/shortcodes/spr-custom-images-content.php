<?php
// Services Shortcode
function sprinklr_content_images_shortcode_function($atts, $content = null){
   extract(shortcode_atts(array(
      			'class'				=> '',
				'id'				=> '',
				'section_top_image' => '',
				'title'				=> '',
				'description'		=> '',
				'button_text' 		=> '',
				'button_link' 		=> '',
				'textcolor'			=> '#fff',
				'target'			=> '_blank',
   ), $atts));

	$spr_attr_id = '';
	if(!empty($id)){
		$spr_attr_id='id="'.$id.'"';
	}
	$class_attr= '';
	if(!empty($class)){
		$class_attr='class="'.$class.'"';
	}
	$spr_style = '';
	if(isset($textcolor) && !empty($textcolor)){
		$spr_style .= 'color: '.$textcolor.';';
	}
	$spr_style_attr='';
	if(!empty($spr_style)){
		$spr_style_attr='style="'.$spr_style.'"';
	}
	$return_string = '<div class="social-expeirence text-center '.$class_attr.'" '.$spr_attr_id.' '.$spr_style_attr.' >';
		if(isset($section_top_images) && !empty($section_top_images)){
   			$return_string .= '<div class="spr-top-image">
					   			<img class="spr-aligncenter size-full" alt="" src="'.$section_top_images.'">
					   		</div>';
		}
	   		if(isset($title) && !empty($title)){
	   			$return_string .= '<h2>'.$title.'</h2>';
	   		}
	   		if(isset($description) && !empty($description)){
	   			$return_string .= '<strong>'.$description.'</strong>';
	   		}
   			$return_string .= '<ul class="social-experience-list">';
   			if(isset($content) && !empty($content)){
   				if(function_exists('spr_content_strip_br')){
   					$content  = spr_content_strip_br($content);
   				}
   				$return_string .= do_shortcode($content);
   			}
   			$return_string .= '</ul>';
   			if(isset($button_text) && !empty($button_text)){
	   			$return_string .= '<a href="'.$button_link.'" target="'.$target.'" class="btn btn-default spr-bg-hover">'.$button_text.'</a></div>';
	   		}
   			$return_string .= '</div>';
   return $return_string;
}
// Heading Shortcode
function sprinklrspr_content_image_child_shortcode_function($atts, $content = null){
   extract(shortcode_atts(array(
      			'name'			=> '',
				'image'			=> '',
				'image_link'	=> '#',
   ), $atts));
   $image_item = '';
	if((isset($name) && !empty($name)) || (isset($image) && !empty($image))){
	   $image_item .= '<li><a href="'.$image_link.'" >';
		   if(isset($image) && !empty($image)){
		   		$image_item .= '<span><img src="'.$image.'" alt="'.sanitize_title($name).'"></span>';
		   }
		   if(isset($name) && !empty($name)){
		   		$image_item .= '<small>'.$name.'</small>';
		   }
	   $image_item .= '</a></li>';
	}
   return $image_item;
}

add_shortcode('spr_content_image', 'sprinklrspr_content_image_child_shortcode_function');
add_shortcode('spr_content_images', 'sprinklr_content_images_shortcode_function');