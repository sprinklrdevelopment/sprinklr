<?php

/**
 *
 * Shortcode for hubspotform.
 * @param array $atts
 * @return string HTML
 * Usage:
 * [hubspotform title="Hubsport Form" protal_id= form_id=" campaign_id="" webinar_key="" large_button="yes" button_text="Save hubspot Form" css="" class="hubsport-custom-class" id="hubsport-custom-id"]Content[/hubspotform]
 *
 */
function spr_hubpost_handler($atts , $content = null ) {

    $_PORTAL_ID = '457361';

    // Attributes
    extract( shortcode_atts(
        array(
            'section_top_image' => '',
            'title' => '',
            'protal_id' => '',
            'form_id' => '',
            'campaign_id' => '',
            'webinar_key' => '',
            'large_button' => '',
            'button_text' => '',
            'css' => '',
            'class' => '',
            'id' => '',
        ), $atts )
    );

    $target_div_id = "hubspotform-".time();
    $id_att = '';
    if(isset($id) && !empty($id)){
        $id_att = 'id="'.$id.'"';
    }
    $code = "<div ".$id_att."  class='demo-content text-center hupspot-form ".$class."'>";
    if(isset($section_top_image) && !empty($section_top_image)) {
        $code .= '<img src="'.$section_top_image.'" alt="Sprinklr Burst" />';
    }
    if(isset($title) && !empty($title)) {
        $code .= "<h4>$title</h4>";
    }
    if(isset($content) && !empty($content)) {
        $code .= "<h4>$content</h4>";
    }


    $campaignParams = "";
    if(isset($campaign_id) && !empty($campaign_id)) {
        $campaignParams .= " sfdcCampaignId: '".$campaign_id."',";
    }

    if(isset($webinar_key) && !empty($webinar_key)) {
        $campaignParams .= " goToWebinarWebinarKey: '".$webinar_key."',";
    }

    $buttonClass = "btn btn-primary highlight-button highlight-button-blue";
     if(isset($large_button) && !empty($large_button) && $large_button == 'yes') {
        $buttonClass .= "btn btn-primary highlight-button-large";
    }

    $buttonTextChangeScript = '';
    if(isset($button_text) && !empty($button_text) ){
        $buttonTextChangeScript = "$('.hs-button').attr('value', '{$button_text}');";
    }
    $code .= '<div class="demo-form">';

    $code .= " <div id='$target_div_id' ></div>
                    <script src='http://js.hsforms.net/forms/v2.js'></script>
                    <script>
                    jQuery(document).on('ready', function() {
                        hbspt.forms.create({
                            portalId: '$protal_id',
                            formId: '$form_id',
                            target: '#$target_div_id',
                            $campaignParams
                            css:
                            '.hs-form.stacked .field { margin-bottom: 15px; }' +
                            '.hs-form .hs-input { width: 90%; max-width: 500px; border-radius:0; box-shadow: none; height:30px; margin-bottom: 0px }' +
                            ' $css ',
                            onFormReady: function(o,c){
                            console.log('$buttonClass');
                                jQuery('.hs-button').addClass('$buttonClass');
                                $buttonTextChangeScript
                            }
                        });
                    });
                    </script>
            
    ";
    $code .= "</div>' </div>";

    return $code;
}

add_shortcode('hubspotform', 'spr_hubpost_handler');