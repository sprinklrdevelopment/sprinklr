<?php
if(!function_exists('pt_fusion_shortcodes_range')){
    function pt_fusion_shortcodes_range ( $range, $all = true, $default = false, $range_start = 1 ) {
        if( $all ) {
            $number_of_posts['-1'] = 'All';
        }

        if( $default ) {
            $number_of_posts[''] = 'Default';
        }

        foreach( range( $range_start, $range ) as $number ) {
            $number_of_posts[$number] = $number;
        }

        return $number_of_posts;
    }
}

// Taxonomies
if(!function_exists('pt_fusion_shortcodes_categories')){
    function pt_fusion_shortcodes_categories ( $taxonomy, $empty_choice = false, $empty_choice_label = 'Default' ) {
        $post_categories = array();
        if( $empty_choice == true ) {
            $post_categories[''] = $empty_choice_label;
        }

        $get_categories = get_categories('hide_empty=0&taxonomy=' . $taxonomy);

        if( ! is_wp_error( $get_categories ) ) {
            if( $get_categories && is_array($get_categories) ) {
                foreach ( $get_categories as $cat ) {
                    if( array_key_exists('slug', $cat) && 
                        array_key_exists('name', $cat) 
                    ) {
                        $post_categories[$cat->slug] = $cat->name;
                    }
                }
            }

            if( isset( $post_categories ) ) {
                return $post_categories;
            }
        }
    }
}

if(!function_exists('pt_get_sidebars')){
    function pt_get_sidebars() {
        global $wp_registered_sidebars;

        $sidebars = array();

        foreach( $wp_registered_sidebars as $sidebar_id => $sidebar ) {
            $sidebars[$sidebar_id] = $sidebar['name'];
        }

        return $sidebars;
    }
}
/*-----------------------------------------------------------------------------------*/
/*  Default Options
/*-----------------------------------------------------------------------------------*/
$choices = array( 'yes' => __('Yes', 'fusion-core'), 'no' => __('No', 'fusion-core') );
$reverse_choices = array( 'no' => __('No', 'fusion-core'), 'yes' => __('Yes', 'fusion-core') );
$choices_with_default = array( '' => __('Default', 'fusion-core'), 'yes' => __('Yes', 'fusion-core'), 'no' => __('No', 'fusion-core') );
$reverse_choices_with_default = array( '' => __('Default', 'fusion-core'), 'no' => __('No', 'fusion-core'), 'yes' => __('Yes', 'fusion-core') );
$leftright = array( 'left' => __('Left', 'fusion-core'), 'right' => __('Right', 'fusion-core') );
$dec_numbers = array( '0.1' => '0.1', '0.2' => '0.2', '0.3' => '0.3', '0.4' => '0.4', '0.5' => '0.5', '0.6' => '0.6', '0.7' => '0.7', '0.8' => '0.8', '0.9' => '0.9', '1' => '1' );
$animation_type = array(
                    '0'             => __( 'None', 'fusion-core' ),
                    'bounce'         => __( 'Bounce', 'fusion-core' ),
                    'fade'             => __( 'Fade', 'fusion-core' ),
                    'flash'         => __( 'Flash', 'fusion-core' ),
                    'rubberBand'     => __( 'Rubberband', 'fusion-core' ),                    
                    'shake'            => __( 'Shake', 'fusion-core' ),
                    'slide'         => __( 'Slide', 'fusion-core' ),
                    'zoom'             => __( 'Zoom', 'fusion-core' ),
                );
$animation_direction = array(
                    'down'         => __( 'Down', 'fusion-core' ),
                    'left'         => __( 'Left', 'fusion-core' ),
                    'right'     => __( 'Right', 'fusion-core' ),
                    'up'         => __( 'Up', 'fusion-core' ),
                    'static'     => __( 'Static', 'fusion-core' ),
                );

// Fontawesome icons list
$pattern = '/\.(fa-(?:\w+(?:-)?)+):before\s+{\s*content:\s*"(.+)";\s+}/';
$fontawesome_path = FUSION_TINYMCE_DIR . '/css/font-awesome.css';
if( file_exists( $fontawesome_path ) ) {
    @$subject = file_get_contents( $fontawesome_path );
}


preg_match_all($pattern, $subject, $matches, PREG_SET_ORDER);

$icons = array();

foreach($matches as $match){
    $icons[$match[1]] = $match[2];
}

$checklist_icons = array ( 'icon-check' => '\f00c', 'icon-star' => '\f006', 'icon-angle-right' => '\f105', 'icon-asterisk' => '\f069', 'icon-remove' => '\f00d', 'icon-plus' => '\f067' );

/*-----------------------------------------------------------------------------------*/
/*  Shortcode Selection Config
/*-----------------------------------------------------------------------------------*/
global $fusion_shortcodes;

/*-----------------------------------------------------------------------------------*/
/*  Content with button Config
/*-----------------------------------------------------------------------------------*/

$fusion_shortcodes['spr_content_button'] = array(
    'no_preview' => true,
    'params' => array(
        'image' => array(
            'std' => '',
            'type' => 'uploader',
            'label' => __( 'Image ', 'fusion-core' ),
            'desc' => __( 'custom Image', 'fusion-core')
        ),
        'title' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Title', 'fusion-core' ),
            'desc' => __( 'Section Title', 'fusion-core')
        ),
        
        'short_description' => array(
            'std' => '',
            'type' => 'textarea',
            'label' => __( 'Short Description', 'fusion-core' ),
            'desc' => __( '', 'fusion-core')
        ),
       
        'button_text' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Button Text', 'fusion-core' ),
            'desc' => __( 'Button Text', 'fusion-core')
        ),

        'button_link' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Button Link', 'fusion-core' ),
            'desc' => __( 'Button Link', 'fusion-core')
        ),
        
        'class' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'CSS Class', 'fusion-core' ),
            'desc' => __( 'Add a class to the wrapping HTML element.', 'fusion-core')
        ),
        'id' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'CSS ID', 'fusion-core' ),
            'desc' => __( 'Add an ID to the wrapping HTML element.', 'fusion-core')
        ),      
    ),
    'shortcode' => '[spr_content_button image={{image}} title="{{title}}" button_text="{{button_text}}" button_link="{{button_link}}" class="{{class}}" id="{{id}}"]{{short_description}}[/spr_content_button]',
    'popup_title' => __( 'Sprinklr Home Experience Cloud Shortcode', 'fusion-core' )
);
/*-----------------------------------------------------------------------------------*/
/*  Benchmark Config
/*-----------------------------------------------------------------------------------*/

$fusion_shortcodes['spr_benchmark'] = array(
    'no_preview' => true,
    'params' => array(
        'heading_text' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Heading', 'fusion-core' ),
            'desc' => __( 'Heading', 'fusion-core')
        ),
        'icon' => array(
                'type' => 'iconpicker',
                'label' => __( 'Select Icon', 'fusion-core' ),
                'desc' => __( 'Display an icon next to title. Click an icon to select, click again to deselect.', 'fusion-core' ),
                'options' => $icons
        ),
        'title' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Title', 'fusion-core' ),
            'desc' => __( 'Section Title', 'fusion-core')
        ),
        'short_description' => array(
            'std' => '',
            'type' => 'textarea',
            'label' => __( 'Short Description', 'fusion-core' ),
            'desc' => __( '', 'fusion-core')
        ),
       
        'button_text' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Button Text', 'fusion-core' ),
            'desc' => __( 'Button Text', 'fusion-core')
        ),

        'button_link' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Button Link', 'fusion-core' ),
            'desc' => __( 'Button Link', 'fusion-core')
        ),
        
        'class' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'CSS Class', 'fusion-core' ),
            'desc' => __( 'Add a class to the wrapping HTML element.', 'fusion-core')
        ),
        'id' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'CSS ID', 'fusion-core' ),
            'desc' => __( 'Add an ID to the wrapping HTML element.', 'fusion-core')
        ),      
    ),
    'shortcode' => '[spr_benchmark heading_text="{{heading_text}}" title="{{title}}" icon={{icon}} button_text="{{button_text}}" button_link="{{button_link}}" class="{{class}}" id="{{id}}"]{{short_description}}[/spr_benchmark]',
    'popup_title' => __( 'Benchmark Shortcode', 'fusion-core' )
);
/*-----------------------------------------------------------------------------------*/
/*  Hubspot Config
/*-----------------------------------------------------------------------------------*/

$fusion_shortcodes['hubspot'] = array(
    'no_preview' => true,
    'params' => array(

        'section_top_image' => array(
            'type' => 'uploader',
            'label' => __( 'Custom Image', 'fusion-core' ),
            'desc' => __( 'Upload a section top image.', 'fusion-core' ),
        ),
        'title' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Title', 'fusion-core' ),
            'desc' => __( 'Section Title', 'fusion-core')
        ),
        'short_description' => array(
            'std' => '',
            'type' => 'textarea',
            'label' => __( 'Short Description', 'fusion-core' ),
            'desc' => __( '', 'fusion-core')
        ),
        'protal_id' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Portal ID', 'fusion-core' ),
            'desc' => __( 'Hubspot Portal ID', 'fusion-core')
        ),
        'form_id' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Form ID', 'fusion-core' ),
            'desc' => __( 'Hubspot Form ID', 'fusion-core')
        ),
        'campaign_id' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Campaign ID', 'fusion-core' ),
            'desc' => __( 'Hubspot Campaign ID', 'fusion-core')
        ),
        'webinar_key' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Webinar Key', 'fusion-core' ),
            'desc' => __( 'Hubspot Webinar Key', 'fusion-core')
        ),
        'large_button' => array(
            'std' => '',
            'type' => 'select',
            'label' => __( 'Large Button', 'fusion-core' ),
            'desc' => __( 'Large Button ', 'fusion-core'),
            'options' => $choices
        ),
        'button_text' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Button Text', 'fusion-core' ),
            'desc' => __( 'Button Text', 'fusion-core')
        ),
        'css' => array(
            'std' => '',
            'type' => 'textarea',
            'label' => __( 'CSS', 'fusion-core' ),
            'desc' => __( 'Custom CSS ', 'fusion-core')
        ),
        
        'class' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'CSS Class', 'fusion-core' ),
            'desc' => __( 'Add a class to the wrapping HTML element.', 'fusion-core')
        ),
        'id' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'CSS ID', 'fusion-core' ),
            'desc' => __( 'Add an ID to the wrapping HTML element.', 'fusion-core')
        ),      
    ),
    'shortcode' => '[hubspotform section_top_image="{{section_top_image}}" title="{{title}}" protal_id={{protal_id}} form_id="{{form_id}}" campaign_id="{{campaign_id}}" webinar_key="{{webinar_key}}" large_button="{{large_button}}" button_text="{{button_text}}"  css="{{css}}" class="{{class}}" id="{{id}}"]{{short_description}}[/hubspotform]',
    'popup_title' => __( 'Hupspot Shortcode', 'fusion-core' )
);

/*-----------------------------------------------------------------------------------*/
/*  Title Config
/*-----------------------------------------------------------------------------------*/

$fusion_shortcodes['heading'] = array(
    'no_preview' => true,
    'params' => array(
        'content' => array(
            'std' => '',
            'type' => 'textarea',
            'label' => __( 'Heading Text', 'fusion-core' ),
            'desc' => __( 'Insert the heading text', 'fusion-core' ),
        ),
        'size' => array(
            'type' => 'select',
            'label' => __( 'Title Size', 'fusion-core' ),
            'desc' => __( 'Choose the title size, H1-H6', 'fusion-core' ),
            'options' => pt_fusion_shortcodes_range( 6, false )
        ),
        'contentalign' => array(
            'type' => 'select',
            'label' => __( 'Heading Alignment', 'fusion-core' ),
            'desc' => __( 'Choose to align the heading left, center or right.', 'fusion-core' ),
            'options' => array(
                'left' => __('Left', 'fusion-core'),
                'center' => __('Center', 'fusion-core'),
                'right' => __('Right', 'fusion-core')
            )
        ),     
        
          
        'color' => array(
            'type' => 'colorpicker',
            'label' => __( 'Color', 'fusion-core' ),
            'desc' => __( 'Controls the text color.  Leave blank for theme option selection.', 'fusion-core')
        ),

        'font_size' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Font Size', 'fusion-core' ),
            'desc' => __( 'Heading Font Size. In px or em, e.g. 30px.', 'fusion-core' )
        ),
        'font_weight' => array(
            'type' => 'select',
            'label' => __( 'Font Weight', 'fusion-core' ),
            'desc' => __( 'Choose font weight.', 'fusion-core' ),
            'options' => array(
                'bold' => __('Bold', 'fusion-core'),
                'normal' => __('Normal', 'fusion-core'),
            )
        ),
        'heading_border' => array(
            'type' => 'select',
            'label' => __( 'Enable Bottom Border', 'fusion-core' ),
            'desc' => __( '', 'fusion-core' ),
            'options' => array(
                'no' => __('No', 'fusion-core'),
                'yes' => __('yes', 'fusion-core'),
            )
        ),
        'line_height' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Lign Height', 'fusion-core' ),
            'desc' => __( 'Heading Line Height. In px or em, e.g. 50px.', 'fusion-core' )
        ),
        'margin_top' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Top Margin', 'fusion-core' ),
            'desc' => __( 'Spacing above the title. In px or em, e.g. 10px.', 'fusion-core' )
        ),
        'margin_bottom' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Bottom Margin', 'fusion-core' ),
            'desc' => __( 'Spacing below the title. In px or em, e.g. 10px.', 'fusion-core' )
        ),          
        
        'class' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'CSS Class', 'fusion-core' ),
            'desc' => __( 'Add a class to the wrapping HTML element.', 'fusion-core' )
        ),
        'id' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'CSS ID', 'fusion-core' ),
            'desc' => __( 'Add an ID to the wrapping HTML element.', 'fusion-core' )
        ),          
    ),
    'shortcode' => '[heading size="{{size}}" font_weight="{{font_weight}}" content_align="{{contentalign}}" font_size="{{font_size}}" color="{{color}}" heading_border="{{heading_border}}" line_height="{{line_height}}" margin_top="{{margin_top}}" margin_bottom="{{margin_bottom}}" class="{{class}}" id="{{id}}"]{{content}}[/heading]',
    'popup_title' => __( 'Heading Shortcode', 'fusion-core' )
);
/*-----------------------------------------------------------------------------------*/
/*  Services Config
/*-----------------------------------------------------------------------------------*/

$fusion_shortcodes['services'] = array(
    'no_preview' => true,
    'params' => array(
        
        'title' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Title', 'fusion-core' ),
            'desc' => __( 'Add a class to the wrapping HTML element.', 'fusion-core' )
        ),
        'description' => array(
            'std' => '',
            'type' => 'textarea',
            'label' => __( 'Description', 'fusion-core' ),
            'desc' => __( 'Add a class to the wrapping HTML element.', 'fusion-core' )
        ),
        'section_top_icon' => array(
                'type' => 'iconpicker',
                'label' => __( 'Select Icon', 'fusion-core' ),
                'desc' => __( 'Upload a custom Section Top Icon. Click an icon to select, click again to deselect.', 'fusion-core' ),
                'options' => $icons
        ),
        
        'textcolor' => array(
            'type' => 'colorpicker',
            'std' => '',
            'label' => __( 'Text Color', 'fusion-core' ),
            'desc' => __( 'Controls the text color. Leave blank for theme option selection.', 'fusion-core' ),
        ),
             
        'class' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'CSS Class', 'fusion-core' ),
            'desc' => __( 'Add a class to the wrapping HTML element.', 'fusion-core' )
        ),
        'id' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'CSS ID', 'fusion-core' ),
            'desc' => __( 'Add an ID to the wrapping HTML element.', 'fusion-core' )
        ),      
    ),
    'shortcode' => '[services title="{{title}}" description="{{description}}" section_top_icon="{{section_top_icon}}" textcolor="{{textcolor}}" class="{{class}}" id="{{id}}"]{{child_shortcode}}[/services]',
    'popup_title' => __( 'Insert Services Shortcode', 'fusion-core' ),

    'child_shortcode' => array(
        'params' => array(
            'name' => array(
                'std' => '',
                'type' => 'text',
                'label' => __( 'Name', 'fusion-core' ),
                'desc' => __( 'Insert the title.', 'fusion-core' ),
            ),
            
            'image' => array(
                'type' => 'uploader',
                'label' => __( 'Custom Image', 'fusion-core' ),
                'desc' => __( 'Upload a custom image.', 'fusion-core' ),
            ),
            
            'link' => array(
                'std' => '',
                'type' => 'text',
                'label' => __( 'Link', 'fusion-core' ),
                'desc' => __( 'Add the url that will link to.', 'fusion-core' ),
            ),
            'target' => array(
                'type' => 'select',
                'label' => __( 'Target', 'fusion-core' ),
                'desc' => __( '_self = open in same window <br />_blank = open in new window.', 'fusion-core' ),
                'options' => array(
                    '_self' => '_self',
                    '_blank' => '_blank'
                )
            ),
            'content' => array(
                'std' => '',
                'type' => 'textarea',
                'label' => __( 'Services Content', 'fusion-core' ),
                'desc' => __( 'Add the Services content', 'fusion-core' ),
            )
        ),
        'shortcode' => '[service name="{{name}}"  image="{{image}}" link="{{link}}" target="{{target}}"]{{content}}[/service]',
        'clone_button' => __( 'Add service', 'fusion-core' )
    )
);
// Sprinklr Carousel shortcode
$fusion_shortcodes['spr_carousel'] = array(
    'no_preview' => true,
    'params' => array(
       
        'title' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Title', 'fusion-core' ),
            'desc' => __( 'Add a class to the wrapping HTML element.', 'fusion-core' )
        ),
       
        'class' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'CSS Class', 'fusion-core' ),
            'desc' => __( 'Add a class to the wrapping HTML element.', 'fusion-core' )
        ),
        'id' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'CSS ID', 'fusion-core' ),
            'desc' => __( 'Add an ID to the wrapping HTML element.', 'fusion-core' )
        ),      
    ),
    'shortcode' => '[spr_carousel title="{{title}}" class="{{class}}" id="{{id}}"]{{child_shortcode}}[/spr_carousel]',
    'popup_title' => __( 'Insert Sprinklr Carousel Shortcode', 'fusion-core' ),

    'child_shortcode' => array(
        'params' => array(
            'name' => array(
                'std' => '',
                'type' => 'text',
                'label' => __( 'Name', 'fusion-core' ),
                'desc' => __( 'Insert the title.', 'fusion-core' ),
            ),
            
            'image' => array(
                'type' => 'uploader',
                'label' => __( 'Custom Image', 'fusion-core' ),
                'desc' => __( 'Upload a custom image.', 'fusion-core' ),
            ),
            'image_link' => array(
                'std' => '',
                'type' => 'text',
                'label' => __( 'Image Link', 'fusion-core' ),
                'desc' => __( 'Add the url for image.', 'fusion-core' ),
        ),
            
        ),
        'shortcode' => '[spr_carousel_image name="{{name}}" image="{{image}}" image_link="{{image_link}}"][/spr_carousel_image]',
        'clone_button' => __( 'Add Image', 'fusion-core' )
    )
);
// Sprinklr Content with images shortcode
$fusion_shortcodes['spr_content_images'] = array(
    'no_preview' => true,
    'params' => array(
        'section_top_image' => array(
                'type' => 'uploader',
                'label' => __( 'Custom Image', 'fusion-core' ),
                'desc' => __( 'Upload a custom image at top of content.', 'fusion-core' ),
        ),
        'title' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'Title', 'fusion-core' ),
            'desc' => __( 'Add a class to the wrapping HTML element.', 'fusion-core' )
        ),
        'description' => array(
            'std' => '',
            'type' => 'textarea',
            'label' => __( 'Description', 'fusion-core' ),
            'desc' => __( 'Add a class to the wrapping HTML element.', 'fusion-core' )
        ),
        
        'textcolor' => array(
            'type' => 'colorpicker',
            'std' => '',
            'label' => __( 'Text Color', 'fusion-core' ),
            'desc' => __( 'Controls the text color. Leave blank for theme option selection.', 'fusion-core' ),
        ),

        'button_text' => array(
            'type' => 'text',
            'std' => '',
            'label' => __( 'Button Text', 'fusion-core' ),
            'desc' => __( 'Button Text.', 'fusion-core' ),
        ),

        'button_link' => array(
                'std' => '',
                'type' => 'text',
                'label' => __( 'Button Link', 'fusion-core' ),
                'desc' => __( 'Add the url for button.', 'fusion-core' ),
        ),
        'target' => array(
                'type' => 'select',
                'label' => __( 'Target', 'fusion-core' ),
                'desc' => __( '_self = open in same window <br />_blank = open in new window.', 'fusion-core' ),
                'options' => array(
                    '_self' => '_self',
                    '_blank' => '_blank'
                )
        ),
        'class' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'CSS Class', 'fusion-core' ),
            'desc' => __( 'Add a class to the wrapping HTML element.', 'fusion-core' )
        ),
        'id' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'CSS ID', 'fusion-core' ),
            'desc' => __( 'Add an ID to the wrapping HTML element.', 'fusion-core' )
        ),      
    ),
    'shortcode' => '[spr_content_images title="{{title}}" section_top_image="{{section_top_image}}" description="{{description}}" textcolor="{{textcolor}}" button_text="{{button_text}}" button_link="{{button_link}}" target="{{target}}" class="{{class}}" id="{{id}}"]{{child_shortcode}}[/spr_content_images]',
    'popup_title' => __( 'Insert Sprinklr Content Shortcode', 'fusion-core' ),

    'child_shortcode' => array(
        'params' => array(
            'name' => array(
                'std' => '',
                'type' => 'text',
                'label' => __( 'Name', 'fusion-core' ),
                'desc' => __( 'Insert the title.', 'fusion-core' ),
            ),
            
            'image' => array(
                'type' => 'uploader',
                'label' => __( 'Custom Image', 'fusion-core' ),
                'desc' => __( 'Upload a custom image.', 'fusion-core' ),
            ),
            'image_link' => array(
                'std' => '',
                'type' => 'text',
                'label' => __( 'Image Link', 'fusion-core' ),
                'desc' => __( 'Add the url for image.', 'fusion-core' ),
        ),
            
        ),
        'shortcode' => '[spr_content_image name="{{name}}" image="{{image}}" image_link="{{image_link}}"][/spr_content_image]',
        'clone_button' => __( 'Add Image', 'fusion-core' )
    )
);
/*-----------------------------------------------------------------------------------*/
/*  Image Tabs Config
/*-----------------------------------------------------------------------------------*/

$fusion_shortcodes['spr_image_tabs'] = array(
    'no_preview' => true,
    'params' => array(
        'class' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'CSS Class', 'fusion-core' ),
            'desc' => __( 'Add a class to the wrapping HTML element.', 'fusion-core' )
        ),
        'id' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'CSS ID', 'fusion-core' ),
            'desc' => __( 'Add an ID to the wrapping HTML element.', 'fusion-core' )
        ),      
    ),
    'shortcode' => '[spr_image_tabs class="{{class}}" id="{{id}}"]{{child_shortcode}}[/spr_image_tabs]',
    'popup_title' => __( 'Insert image tabs Shortcode', 'fusion-core' ),

    'child_shortcode' => array(
        'params' => array(
            'name' => array(
                'std' => '',
                'type' => 'text',
                'label' => __( 'Name', 'fusion-core' ),
                'desc' => __( 'Insert the title.', 'fusion-core' ),
            ),
            'section_top_images' => array(
                'type' => 'uploader',
                'label' => __( 'Section Top Icon', 'fusion-core' ),
                'desc' => __( 'Upload a custom Section Top Icon.', 'fusion-core' ),
            ),
            'sub_heading' => array(
                'std' => '',
                'type' => 'textarea',
                'label' => __( 'Content Subheading', 'fusion-core' ),
                'desc' => __( 'Add the content', 'fusion-core' ),
            ),
            'content' => array(
                'std' => '',
                'type' => 'textarea',
                'label' => __( 'Content', 'fusion-core' ),
                'desc' => __( 'Add the content', 'fusion-core' ),
            ),
            'button_text' => array(
                'std' => 'Learn More',
                'type' => 'text',
                'label' => __( 'Button Text', 'fusion-core' ),
                'desc' => __( '', 'fusion-core' ),
            ),
            'link' => array(
                'std' => '#',
                'type' => 'text',
                'label' => __( 'Button Link', 'fusion-core' ),
                'desc' => __( 'Add the url that will link to.', 'fusion-core' ),
            ),
            'target' => array(
                'type' => 'select',
                'label' => __( 'Target', 'fusion-core' ),
                'desc' => __( '_self = open in same window <br />_blank = open in new window.', 'fusion-core' ),
                'options' => array(
                    '_self' => '_self',
                    '_blank' => '_blank'
                )
            ),
            'content_image' => array(
                'type' => 'file',
                'label' => __( 'Content Custom Image', 'fusion-core' ),
                'desc' => __( 'Upload a custom image.', 'fusion-core' ),
            ),
            
        ),
        'shortcode' => '[spr_image_tab name="{{name}}" section_top_images="{{section_top_images}}" sub_heading="{{sub_heading}}" button_text="{{button_text}}" link="{{link}}" target="{{target}}" content_image="{{content_image}}"]{{content}}[/spr_image_tab]',
        'clone_button' => __( 'Add Image Tab', 'fusion-core' )
    )
);

/*-----------------------------------------------------------------------------------*/
/*  Twitter post link Config
/*-----------------------------------------------------------------------------------*/


$fusion_shortcodes['spr_tweets'] = array(
    'no_preview' => true,
    'params' => array(
       'short_description' => array(
            'std' => '',
            'type' => 'textarea',
            'label' => __( 'Short Description', 'fusion-core' ),
            'desc' => __( '', 'fusion-core')
        ),
        'class' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'CSS Class', 'fusion-core' ),
            'desc' => __( 'Add a class to the wrapping HTML element.', 'fusion-core')
        ),
        'id' => array(
            'std' => '',
            'type' => 'text',
            'label' => __( 'CSS ID', 'fusion-core' ),
            'desc' => __( 'Add an ID to the wrapping HTML element.', 'fusion-core')
        ),
    ),
    'shortcode' => '[spr_tweets class="{{class}}" id="{{id}}"]{{short_description}}[/spr_tweets]',
    'popup_title' => __( 'Tweets Shortcode', 'fusion-core' )
);