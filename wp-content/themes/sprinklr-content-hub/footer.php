<!-- THE HEADER AND FOOTER MENUS ARE JUST PLACEHOLDER IMAGES. AS YOU WILL BE USING THE SAME MENUS FOR THE REST OF THE SITE, YOU CAN SIMPLY PASTE YOUR CODE HERE -->
<footer class="bkg-charcoal">
<div id="FootNav">
 <div class="container">
  <div class="row">


	<div class="col-md-1"></div>

    <div class="col-lg-5">
    <img src="/images/footnavleft.png" class="fleft">
    </div>

    <div class="col-lg-5">
    <img src="/images/footnavright.png" class="fright">
    </div>
    
	<div class="col-md-1"></div>

      
   </div> 
  </div>
</div>
</footer>

<!-- SCRIPTS -- YOU MAY WISH TO CHANGE THESE TO WHATEVER YOU ARE USING THROUGHOUT THE REST OF THE SITE -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="/js/classie.js"></script>
<script src="/js/modernizr.custom.js"></script>


<!--<script src="/js/ie10-viewport-bug-workaround.js"></script> -->

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


<!-- SCROLL THEN FIXED ITEMS (SOCIAL MEDIA AND MENU) -->    
<script>
var main = function(){
	var menu = $('.fixnav')
	
	$(document).scroll(function(){
		if ( $(this).scrollTop() >= 110 ){
		menu.addClass('top')
		} else {
		menu.removeClass('top')
		}
	})
}
$(document).ready(main);
var main = function(){
	var menu = $('.social-media')
	
	$(document).scroll(function(){
		if ( $(this).scrollTop() >= 580 ){
		menu.addClass('top')
		} else {
		menu.removeClass('top')
		}
	})
}
$(document).ready(main);
</script>

<?php wp_footer(); ?> 
</body>
</html>