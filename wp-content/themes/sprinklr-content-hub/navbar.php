<div class="navbar fixnav" >
<div class="col-md-1"></div>

    
<div class="col-md-3">
<form class="subscribe">
<input type="email" placeholder="enter your e-mail"><button class="bkg-blue">subscribe</button>
</form>
</div><!-- /.subscribe -->
 
 
 <!-- CATEGORY TABS -- THIS CODE IS FULLY FUNCTIONAL, THOUGH I'M SURE THERE'S A CLEANER WAY TO CODE THE PHP HERE -->
 <div>   
	<nav class="navbar-collapse collapse">
    <ul class="nav navbar-nav">
        <li class="category-4"><a href="/category/blog/">All</a>
		<?php global $myCat; $myCat = '4'; include (TEMPLATEPATH . '/inc/carousel.php'); wp_reset_query(); ?> 
        </li>
        
        <li class="category-8"><a href="/category/blog/managing-customer-experiences/">Customer Experience</a>
        <?php $myCat = '8'; include (TEMPLATEPATH . '/inc/carousel.php'); wp_reset_query();?> 
        </li>
        
        <li class="category-42"><a href="/category/blog/advertising/">Advertising</a>
        <?php $myCat = '42'; include (TEMPLATEPATH . '/inc/carousel.php'); wp_reset_query();?> 
        </li>
        
        <li class="category-5"><a href="/category/blog/marketing/">Marketing</a>
        <?php $myCat = '5'; include (TEMPLATEPATH . '/inc/carousel.php'); wp_reset_query();?> 
        </li>
        
        <li class="category-33"><a href="/category/blog/social-data/">Social Data</a>
        <?php $myCat = '33'; include (TEMPLATEPATH . '/inc/carousel.php'); wp_reset_query(); ?> 
        </li>
        
        <li class="category-43"><a href="/category/blog/leadership/">Leadership</a>
        <?php $myCat = '43'; include (TEMPLATEPATH . '/inc/carousel.php'); wp_reset_query();?> 
        </li>
        
        <li class="category-47"><a href="/category/blog/leadership/">Customer Care</a>
        <?php $myCat = '47'; include (TEMPLATEPATH . '/inc/carousel.php'); wp_reset_query();?> 
        </li>
     </ul> 
</div>

   
<?php include (TEMPLATEPATH . '/inc/search-bar.php'); ?>
<?php wp_reset_query(); ?>