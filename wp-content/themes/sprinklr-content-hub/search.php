<?php get_header(); ?>
<?php include (TEMPLATEPATH . '/inc/page-header.php'); ?>


<div id="Navbar">
    <div class="container">
      <div class="row">
      <?php wp_reset_query(); ?>
		<?php include (TEMPLATEPATH . '/navbar.php'); ?>
      </div>
    </div>
</div>



<!-- CONTENT -->
<div id="Content"> 
  <div class="container">
  <div class="row">

  <div class="col-md-1"><!-- PADDING --></div>

<div class="col-md-10">


<!-- START NAVTABS --> 


<?php query_posts('post_status=publish&order=DESC&posts_per_page=12');?>
<ul class="articles">
<?php 
	$counter = 0;
	if ( have_posts() ) : while ( have_posts() ) : the_post(); 
	$data = get_post_data(get_the_id());
?>

    <li>
      <article>
        
        <a href="<?php the_permalink(); ?>" rel="bookmark">
          
          <div class="post-thumbnail"><?php the_post_thumbnail('medium', array('class' => 'alignleft')); ?></div>
                    
          <div class="post-content">
          <p class="post-category bkg-<?php $category = get_the_category(); echo $category[1]->slug;?>"><?php $category = get_the_category(); echo $category[1]->cat_name;?></p>
          <h2 class="entry-title"><?php the_title(); ?></h2>
          <p><?php echo substr(get_the_excerpt(), 0,200); ?>[...]</p>
          </div>
          
          <div class="post-meta">
          <p><a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" title="<?php the_author_meta( 'first_name' ); ?> <br>
		  <?php the_author_meta( 'last_name' ); ?>"><?php echo get_avatar( get_the_author_meta( 'ID' ), 30 ); ?></a> 
		  <?php the_time('n/j/y') ?> | <strong><?=round(sizeof(explode(" ", $data->post_content))/250)?></strong> Min Read | <strong>XX</strong> Shares</p>
          </div>
        </a>
      </article>
    </li>


		
		
		<?php endwhile; ?>
</ul>
<?php endif; ?>
</div>
 



<div class="col-md-1"></div>


      
    </div><!-- /.container --> 
  </div><!-- /.row --> 
</div>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>