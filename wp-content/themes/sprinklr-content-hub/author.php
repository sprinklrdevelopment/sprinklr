<?php /* Template Name:  Author */
get_header();
?>
<?php include (TEMPLATEPATH . '/inc/page-header.php'); ?>    


<!-- AUTHOR -->
<div id="Author" class="bkg-dkgrey">
    <div class="container">
        <div class="row">
            <div class="col-md-1 fright"><!-- spacer--></div>
            <div class="col-md-1 fleft"><!-- spacer--></div>

            <div class="col-lg-4 author-info">
                <?php $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author)); ?>

                <!-- .author-avatar -->
                <div class="author-avatar fleft">
                    <?php echo get_avatar(get_the_author_meta('ID'), 175); ?>
                </div><!-- /.author-name -->


                <!-- .author-name -->
                <div class="author-name">
                    <h1 class="author-title"><?php the_author_meta('first_name'); ?> <br><?php the_author_meta('last_name'); ?></h1>
                    <p><?php $twitterHandle = get_the_author_meta('twitter'); ?> <a href="http://www.twitter.com/<?php echo $twitterHandle; ?>" target="_blank"><?php echo $twitterHandle; ?></a><br>
                        <a href="<?php echo get_usermeta($post->post_author, 'user_url'); ?>" target="_blank"><?php echo get_usermeta($post->post_author, 'user_url'); ?></a></p>
                </div><!-- /.author-name -->

            </div><!-- /.author-info -->


            <!-- .author-bio -->
            <div class="author-bio col-lg-6">
                <p><?php the_author_meta('description'); ?></p>
            </div><!-- /.author-bio -->





        </div> 
    </div>
</div>
<!-- /#AUTHOR -->


<!-- CONTENT -->
<div id="Content"> 
    <div class="container">
        <div class="row">


            <div class="col-md-1"></div>

            <div class="col-md-10">
                <ul class="articles">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                            <li>
                                <article>

                                    <a href="<?php the_permalink(); ?>" rel="bookmark">

                                        <div class="post-thumbnail"><?php the_post_thumbnail('medium', array('class' => 'alignleft')); ?></div>

                                        <div class="post-content">
                                            <p class="post-category bkg-<?php $category = get_the_category();
                    echo $category[1]->slug; ?>"><?php $category = get_the_category();
                    echo $category[1]->cat_name; ?></p>
                                            <h2 class="entry-title"><?php the_title(); ?></h2>
                                            <p><?php echo substr(get_the_excerpt(), 0, 200); ?>[...]</p>
                                        </div>

                                        <div class="post-meta">
                                            <p><a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>" title="<?php the_author_meta('first_name'); ?> <br>
        <?php the_author_meta('last_name'); ?>"><?php echo get_avatar(get_the_author_meta('ID'), 30); ?></a> 
        <?php the_time('n/j/y') ?> | <strong><?= round(sizeof(explode(" ", $data->post_content)) / 250) ?></strong> Min Read | <strong>XX</strong> Shares</p>
                                        </div>
                                    </a>
                                </article>
                            </li>

    <?php endwhile; ?>
<?php endif; ?>


                </ul>
            </div><!-- /ARTICLES -->


            <div class="col-md-1"></div>


        </div><!-- /.container --> 
    </div><!-- /.row --> 
</div>



<script>
    progressMonitor = function () {
        var $ = jQuery,
                indicator = $('#progressIndicator'),
                content = $('#Content'),
                height = $(content).height() - 500;
        jQuery(document).on('scroll', function () {
            $(indicator).css('width', ($(document).scrollTop() / height) * 100 + '%');
        });
    }
    defer(progressMonitor);
    function defer(method) {
        if (window.jQuery)
            method()
        else
            setTimeout(function () {
                defer(method)
            }, 50);
    }
</script>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>