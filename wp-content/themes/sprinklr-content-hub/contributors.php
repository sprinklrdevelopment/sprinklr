<?php /* Template Name:  Contributors */  


// variables
$dispersalRange = 5;

// grab token
function getToken($code) {
	$url = 'https://api2.sprinklr.com/prod0/oauth/token?grant_type=authorization_code';
	$data = array('client_id' => 'zqc5zu4qrwzz6uagq3gmtw3r', 'client_secret' => 'rP3v5hj7FgaFgqFFec9NDqrnVDSqjjTnUk8eKBJdpgSDb4qVjgukku9mdrp5AxaG', 'redirect_uri'=>'https://sprinklr.com', 'code' => $code);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch,CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($data));
	$result = curl_exec($ch);
	print $result;
	curl_close($ch);
	exit();
}



$key = "/vJLzATIcUlaTActdlS/Nx2lV4IUyz5JE3YiXfBbrA84NWMyZmVkYmMxZTgwOTVmYzQxMmU4OGY2YTQxZTYwOQ==";


//getDashboard($key,"CM Misc. Queues","Sales Compliments Queue");

function getDashboard($key,$dashboard,$column) {
	$url = 'https://api2.sprinklr.com/prod0/api/v1/dashboard/'.str_replace(" ","%20",$dashboard).'/stream/'.str_replace(" ","%20",$column);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $key,'Key: zqc5zu4qrwzz6uagq3gmtw3r' ));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	print $result;
	
	curl_close($ch);
	exit();	
}

getFeed($key, "5592dc16e4b025ad2906e970");

function getFeed($key,$feedId) {
	$url = 'https://api2.sprinklr.com/prod0/api/v1/stream/'.$feedId.'/feed?rows=20&clientId=4';
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $key,'Key: zqc5zu4qrwzz6uagq3gmtw3r' ));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = json_decode(curl_exec($ch));
	curl_close($ch);
	return $result;
}

if ( false === ( $value = get_transient( 'sprinklr_feed' ) ) ) {   
	delete_transient( 'sprinklr_feed');
	$feed = array_merge(getFeed($key, "5592dc16e4b025ad2906e976"),getFeed($key, "5592dc16e4b025ad2906e96a"));
	set_transient( 'sprinklr_feed', $feed, 12 * HOUR_IN_SECONDS );
}

$feed = get_transient( 'sprinklr_feed');

/*
print '<pre>';

var_dump($feed);


exit();
*/

get_header(); 
include (TEMPLATEPATH . '/inc/page-header.php'); ?>

<div class="container">
	<div class="row">
      <?php wp_reset_query(); ?>
	  <?php include (TEMPLATEPATH . '/navbar.php'); ?>
    </div>
</div>
    
    
 
<!-- AUTHOR -->
<div id="Author" class="bkg-charcoal">
 <div class="container">
  <div class="row">


    
    <!-- .author-avatars -->
	<div class="author-avatars col-lg-12">
    <h2 class="fleft">Featured<br>Contributors</h2>
<div id="authorlist"><ul><?php contributors(); ?></ul></div>
	</div><!-- /.author-name -->


  </div>
  </div>
</div>
<!-- /#AUTHOR -->




<!-- DATA VIZ -->
<div id="DataViz" class="bkg-medgrey">
 <div class="container">
  <div class="row">


    <div class="col-lg-12 center">
    <p>DataViz Area</p>
    </div>


  
   </div> 
  </div>
</div>
<!-- /#DATA VIZ -->




<div id="Content"> 
  <div class="container">
  <div class="row">

<?php query_posts('post_status=publish&order=DESC&posts_per_page=21');?>
<ul class="articles">
<?php 
	$counter = 0;
	$messageCounter = 0;
	$next = rand(2, $dispersalRange);
	if ( have_posts() ) : while ( have_posts() ) : the_post(); 
	$data = get_post_data(get_the_id());
?>

    <li>
      <article>
        
        <a href="<?php the_permalink(); ?>" rel="bookmark">
          
          <div class="post-thumbnail"><?php the_post_thumbnail('medium', array('class' => 'alignleft')); ?>
          <p class="post-category bkg-<?php $category = get_the_category(); echo $category[1]->slug;?>"><?php $category = get_the_category(); echo $category[1]->cat_name;?></p>
          </div>
          
          <div class="post-content">
          
          <h2 class="entry-title"><?php the_title(); ?></h2>
          <p><?php echo substr(get_the_excerpt(), 0,200); ?>[...]</p>
          </div>
          
          <div class="post-meta">
          <p><a href="#" title="<?php the_author_meta( 'first_name' ); ?> <br>
		  <?php the_author_meta( 'last_name' ); ?>"><?php echo get_avatar( get_the_author_meta( 'ID' ), 30 ); ?></a> 
		  <?php the_time('n/j/y') ?> | <strong><?=round(sizeof(explode(" ", $data->post_content))/250)?></strong> Min Read | <strong>XX</strong> Shares</p>
          </div>
        </a>
      </article>
    </li>


<?php 
	
	if ($counter == $next) {
		$next = rand(2, $dispersalRange);
		$counter = 0;
		$message = $feed[$messageCounter];
		
/*
		if (count( $message->mediaList) == 0) {
			$messageCounter++;	
			$message = $feed[$messageCounter];
		}
				if (count( $message->mediaList) == 0) {
			$messageCounter++;	
			$message = $feed[$messageCounter];
		}
				if (count( $message->mediaList) == 0) {
			$messageCounter++;	
			$message = $feed[$messageCounter];
		}
				if (count( $message->mediaList) == 0) {
			$messageCounter++;	
			$message = $feed[$messageCounter];
		}
*/
			
		
		$messageCounter++;
		
		 ?>
		
		
		    <li>
      <article class="<?=$message->snType?>">
        
        <a href="<?=$message->permalink ?>" rel="bookmark">
          
          <?
	        
	        if (count( $message->mediaList) > 0) {
						$media = $message->mediaList[0]->picture;
						
	        ?>
          
          <div class="post-thumbnail" style="background-image:url(<?=$media?>)"></div>
          
          <? } ?>
          
          <div class="post-content">
          <p class="post-category"><?=$message->snType?></p>
          <h2 class="entry-title"><?=$message->message?></h2>

          </div>
          
          <div class="post-meta">
          <p><a href="<?=$message->senderProfile->permalink?>" target="_blank" title=""><img alt="" src="<?=$message->senderProfile->profileImgUrl?>" class="avatar avatar-30 photo grav-hashed" height="30" width="30" originals="30" scale="2"></a> 
		  <?=date("n/j/y",($message->createdTime/1000)) ?> |  Shares</p>
          </div>
        </a>
             
      </article>
    </li>
		
		
		<?php
		}
	
	$counter++;
	
	endwhile; ?>
</ul>

<?php endif; ?>

      
    </div><!-- /.container --> 
  </div><!-- /.row --> 
</div>
<?php wp_reset_query(); ?>


<!-- FOOT NAV -->
<div id="FootNav" class="bkg-charcoal">
 <div class="container">
  <div class="row">


    <div class="col-lg-6">
    <img src="/images/footnavleft.png" class="fleft">
    </div>

    <div class="col-lg-6">
    <img src="/images/footnavright.png" class="fright">
    </div>
    
      
   </div> 
  </div>
</div>
<!-- /#DATA VIZ -->

<?php get_footer(); ?>