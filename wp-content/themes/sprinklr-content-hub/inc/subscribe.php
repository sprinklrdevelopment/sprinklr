<div class="band bkg-green">
<div class="container">
<div class="row">

<div class="col-md-1"></div>
<h6 class="col-md-6">Insights, ideas &amp; Stories to help brands create experiences that customers love.</h6>

<div class="col-md-4 subscribe">
<form><input type="email" placeholder="your email"><button class="bkg-yellow">Subscribe</button></form>
</div><!-- /.subscribe -->

<div class="col-md-1"></div>

</div>
</div>
</div><!-- /.band -->