<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>


<!-- SCRIPTS & STYLES -->
    <!-- Bootstrap core CSS -->
	<link href="/css/bootstrap.css" rel="stylesheet">
	<link href="/css/push.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>    <link rel="shortcut icon" href="/images/favicon.ico">
<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>



<?php wp_head(); ?>
<?php if ( is_404() ) echo '<meta http-equiv="refresh" content="5;URL=/">'?>


</head>
<body <?php body_class(); ?>>