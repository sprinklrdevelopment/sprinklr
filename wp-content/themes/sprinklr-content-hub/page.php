<?php get_header(); ?>
<?php include (TEMPLATEPATH . '/inc/page-header.php'); ?>

<div id="progress"><div id="progressIndicator"></div></div>


<div id="Navbar">
    <div class="container">
      <div class="row">
      <?php wp_reset_query(); ?>
		<?php include (TEMPLATEPATH . '/navbar.php'); ?>
      </div>
    </div>
</div>
    
 
<!-- BANNER -->
<div id="Banner">
 <div class="container">
  <div class="row">
   <?php if ( have_posts() ) : ?>
   <div class="banner" style="background:url(<?=wp_get_attachment_url( get_post_thumbnail_id($post->ID) )?>)">
   </div>
  
   </div> 
  </div>
</div>
<!-- /BANNER -->




<!-- CONTENT -->

<div id="Content">   
  <div class="container">
    <div class="row category-<?php $category = get_the_category(); echo $category[1]->category_nicename;?>">
		<div class="col-md-1 fright"><!-- RIGHT SIDE SPACER --></div>
        
        <div class="col-md-1 social-media">
        <div class="social">
        <a href="http://www.facebook.com" target="_blank"><img src="/images/icon-facebook.png" alt="Facebook"></a>
        <a href="http://www.twitter.com" target="_blank"><img src="/images/icon-twitter.png" alt="Twitter"></a>
        <a href="http://www.linkedin.com" target="_blank"><img src="/images/icon-linkedin.png" alt="LinkedIn"></a>
        </div>
        </div>

        
		<!-- SIDEBAR -->
        <div class="col-md-3 center sidebar fright" id="Sidebar">
  		  <div class="post-meta">
          <?php echo get_avatar( get_the_author_meta( 'ID' ), 175 ); ?>
          <p>by <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><?php the_author_meta( 'first_name' ); ?> <?php the_author_meta( 'last_name' ); ?></a><br>
			<?php $twitterHandle = get_the_author_meta('twitter');?> <a href="http://www.twitter.com/<?php echo $twitterHandle ;?>" target="_blank"><?php echo $twitterHandle ;?></a></p>
          </div>
          
          
  		  <!-- RELATED SIDEBAR POST -->
          <div class="related">
          <p><em>you might also like</em></p>
          
            <ul><!-- YOU MAY NEED TO ADJUST THIS CODE TO DRAW IN RELATED CONTENT AS DESIRED. THIS CURRENTLY PULLS IN CONTENT FROM THE LATEST POST IN THE SAME CATEGORY, EXCLUDING THE CURRENT POST -->
                <?php wp_reset_query();
				$thiscat = get_query_var('cat'); query_posts( array (post_status => publish, order => DESC, posts_per_page =>1, cat => $thiscat, 'post__not_in' => array($post->ID)));?>
                <?php while ( have_posts() ) : the_post(); ?>
                <li>
                <div class="post-thumbnail"><?php the_post_thumbnail('large', array('class' => 'alignleft')); ?></div>
                
                <div class="entry">
                <p class="post-category"><?php $category = get_the_category(); echo $category[0]->cat_name;?></p>
                <div class="entry-title">
                <h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
                </div>
                </li>
                <?php endwhile; wp_reset_query(); ?>
            </ul>
		  </div>
          
                    
        </div><!-- END SIDEBAR --> 


		<!-- ARTICLE -->
          <article>      
            <?php while ( have_posts() ) : the_post(); ?>
            <h1 class="entry-title">
              <?php the_title(); ?>
            </h1>
            
            <div class="post-meta">
            <p><?php the_time('l, F jS, Y') ?> | <strong>XX</strong> min read | <strong>421</strong> shares | <img src="/images/trending.jpg"></p>
            </div>
            
            <?php the_content(); ?>
            
            <p class="continue">Continue the conversation on&nbsp;&nbsp;&nbsp;<a href="http://www.facebook.com" target="_blank"><img src="/images/icon-facebook.png" alt="Facebook"></a> <a href="http://www.twitter.com" target="_blank"><img src="/images/icon-twitter.png" alt="Twitter"></a> <a href="http://www.linkedin.com" target="_blank"><img src="/images/icon-linkedin.png" alt="LinkedIn"></a></p>
            <?php endwhile; ?>
            <?php endif; ?>
          </article>
        
        

    </div><!-- /container --> 
  </div><!-- /row --> 
</div><!-- /Content -->


<!-- SUBSCRIBE -->
<div class="band bkg-green">
<div class="container">
<div class="row">

<div class="col-md-1"></div>
<div class="col-md-6"><h6>Get access to industry-leading social content</h6></div>

<div class="col-md-4 subscribe">
<form><input type="email" placeholder="your email"><button class="bkg-yellow">Subscribe</button></form>
</div><!-- /.subscribe -->

<div class="col-md-1"></div>

</div>
</div>
</div><!-- /.band -->





<div id="Related" class="bkg-grey">
  <div class="container">
    <div class="row">
    
    <div class="col-md-1">&nbsp;</div>
    
    <div class="col-md-7 related">
    <h5><strong>you'll probably also like...</strong></h5>
    
    <ul>
        

	  <?php query_posts('post_status=publish&order=DESC&posts_per_page=2');?>
            <?php while ( have_posts() ) : the_post(); ?>
            <li>
          <div class="post-thumbnail col-md-5"><?php the_post_thumbnail('large', array('class' => 'alignleft')); ?></div>
            
            <div class="excerpt col-md-6">
            <p class="post-category"><?php $category = get_the_category(); echo $category[0]->cat_name;?></p>
            <div class="entry-title">
            <h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
            </div>
            
            
          <section id="Meta">
          
          <div class="post-meta"><p><a href="#" title="<?php the_author_meta( 'first_name' ); ?> <?php the_author_meta( 'last_name' ); ?>"><?php echo get_avatar( get_the_author_meta( 'ID' ), 40 ); ?></a> <strong>15</strong> min read | <strong>421</strong> Shares</p></div>
          </section>   
          </div>
</li>
            <?php endwhile; ?>
          


        </ul>
    </div>
    
   <div class="col-md-1 fright">&nbsp;</div>

    
    <div class="col-md-3 recent fright">
    <h5><strong>recently published</strong></h5>
    <ul>
     <?php query_posts('post_status=publish&order=DESC&posts_per_page=3');?>
        <?php while ( have_posts() ) : the_post(); ?>
          <li>            
            <p class="post-category"><?php $category = get_the_category(); echo $category[0]->cat_name;?></p>
            <div class="entry-title">
            <h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
            </div>
            
            
          <section id="Meta">
          <div class="post-meta"><p><strong>15</strong> min read | <strong>421</strong> Shares</p></div>
          </section>   
		</li>
        <?php endwhile; ?>
       </ul>   
    </div>    
    
    
    </div>
   </div>
</div>
<script>
	

	
	progressMonitor = function() {
	var $ = jQuery,
	indicator = $('#progressIndicator'),
	content = $('#Content'),
	height = $(content).height() - 500;


	jQuery(document).on( 'scroll', function(){
		$(indicator).css('width',($(document).scrollTop()/height)*100 + '%');
	});
	
	}
	
	defer(progressMonitor);
	
	function defer(method) {
    if (window.jQuery)
        method()	
    else
        setTimeout(function() { defer(method) }, 50);
}
	
	
	
</script>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>