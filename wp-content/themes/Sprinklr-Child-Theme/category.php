<?php
get_header('blog');
$dispersalRange = 5;
$key = "/vJLzATIcUlaTActdlS/Nx2lV4IUyz5JE3YiXfBbrA84NWMyZmVkYmMxZTgwOTVmYzQxMmU4OGY2YTQxZTYwOQ==";
$feed = get_transient('sprinklr_feed');
?>
<!-- BANNER -->
<div id="Banner">
    <div class="container">
        <div class="row">
            <ul class="trending" id="Trending">
                <?php
                $query_args = array(
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    'posts_per_page' => 3,
                    'order' => 'DESC',
                    'orderby' => 'date',
                    'post__in' => get_option('sticky_posts'),
                );
                $the_query = new WP_Query($query_args);
                if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
                
                    $data = get_post_data(get_the_id());
                    ?>
                    <li>
                        <div class="post-thumbnail"><?php the_post_thumbnail('full-size', array('class' => 'alignleft')); ?></div>
                        <article>
                            <?php
                            $before_cat = '<p class="post-category">';
                            $categories_list = get_the_term_list ( get_the_id(), 'category', $before_cat , ', ', '</p>' );
                            if ( $categories_list ){
                                  printf( __( '%1$s', 'Sprinklr'),$categories_list );
                            } 
                           ?>

                            <div class="entry-title">
                                <h1><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(sprintf(__('Permalink to %s', 'Sprinklr'), the_title_attribute('echo=0'))); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
                            </div>
                            <section id="Meta">
                                <div class="post-meta"><p><a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>" title="<?php the_author_meta('first_name'); ?> <?php the_author_meta('last_name'); ?>"><?php echo get_avatar(get_the_author_meta('ID'), 80); ?></a> <?php
                                        //$userlogo = get_the_author_meta('logo');
                                       // echo '<img src="' . $userlogo . '" />';
                                        ?> <strong><?php echo round(sizeof(explode(" ", $data->post_content)) / 250) ?></strong> <?php esc_attr_e('min read', 'Sprinklr'); ?>
                                </div>
                            </section>   

                        </article>
                    </li>
                <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
            </ul>

        </div> 
    </div>
</div>



<!-- /NAVBAR -->
<div id="Navbar">
    <div class="container">
        <div class="row">

            <?php include_once get_stylesheet_directory() . '/inc/navbar-home.php'; ?>
        </div>
    </div>
</div>

<!-- CONTENT -->
<div id="Content"> 
    <div class="container">
        <div class="row">
            <div class="col-md-1 fleft"></div>


            <!-- START TABS --> 
            <div class="col-md-10">
                <div class="tab-content">
                    <div class="tab-pane active fade in" id="All">
                        <?php
                        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

                        $query_args = array(
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'posts_per_page' => get_option('posts_per_page'),
                            'order' => 'DESC',
                            'orderby' => 'date',
                            'post__not_in' => get_option('sticky_posts'), 
                            'ignore_sticky_posts' => 1,
                            'paged' => $paged
                        );
                        $the_query = new WP_Query($query_args);
                        ?>

                        <ul class="articles">
                            <?php
                            $counter = 0;
                            $messageCounter = 0;
                            $next = rand(2, $dispersalRange);
                            if ($the_query->have_posts()) : while ($the_query->have_posts()) : $the_query->the_post();
                                    $data = get_post_data(get_the_id());
                                    ?>

                                    <li>
                                        <article>

                                            <a href="<?php the_permalink(); ?>" rel="bookmark">

                                                <div class="post-thumbnail"><?php the_post_thumbnail('medium', array('class' => 'alignleft')); ?></div>

                                                <div class="post-content">
                                                   <?php
                                                    $before_cat = '<p class="post-category">';
                                                    $categories_list = get_the_term_list ( get_the_id(), 'category', $before_cat , ', ', '</p>' );
                                                    if ( $categories_list ){
                                                          printf( __( '%1$s', 'Sprinklr'),$categories_list );
                                                    } 
                                                   ?>
                                                    <h2 class="entry-title"><?php the_title(); ?></h2>
                                                    <p><?php echo substr(get_the_excerpt(), 0, 200); ?>[...]</p>
                                                </div>

                                                <div class="post-meta">
                                                    <p><a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>" title="<?php the_author_meta('first_name'); ?> <br>
                                                          <?php the_author_meta('last_name'); ?>"><?php echo get_avatar(get_the_author_meta('ID'), 30); ?></a> 
                                                        <?php the_time('n/j/y') ?> | <strong><?php echo round(sizeof(explode(" ", $data->post_content)) / 250) ?></strong> <?php esc_attr_e('Min Read', 'Sprinklr'); ?></p>
                                                </div>
                                            </a>
                                        </article>
                                    </li>


                                    <?php
                                    if ($counter == $next) {
                                        $next = rand(2, $dispersalRange);
                                        $counter = 0;
                                        if (isset($feed[$messageCounter]) && !empty($feed[$messageCounter])) {
                                            $message = $feed[$messageCounter];
                                            $messageCounter++;
                                            ?>


                                            <li>
                                                <article class="<?php echo $message->snType ?>">

                                                    <a href="<?php echo $message->permalink ?>" rel="bookmark">

                                                        <?php
                                                        if (count($message->mediaList) > 0) {
                                                            $media = $message->mediaList[0]->picture;
                                                            ?>
                                                            <div class="post-thumbnail" style="background-image:url(<?php echo $media ?>)"></div>
                                                        <?php } ?>

                                                        <div class="post-content">
                                                            <p class="post-category"><?php echo $message->snType ?></p>
                                                            <h2 class="entry-title"><?php echo $message->message ?></h2>

                                                        </div>

                                                        <div class="post-meta">
                                                            <p><a href="<?php echo $message->senderProfile->permalink ?>" target="_blank" title=""><img alt="" src="<?php echo $message->senderProfile->profileImgUrl ?>" class="avatar avatar-30 photo grav-hashed" height="30" width="30" originals="30" scale="2"></a> 
                                                                <?php echo date("n/j/y", ($message->createdTime / 1000)) ?></p>
                                                        </div>
                                                    </a>

                                                </article>
                                            </li>


                                            <?php
                                        }
                                    } $counter++;
                                endwhile;
                                ?>
                            </ul>
                            <div class="clearfix"></div>
                            <?php
                            if (function_exists('sprinklr_custom_pagination')) {
                                sprinklr_custom_pagination($the_query->max_num_pages, "", $paged);
                            }
                            ?>
                        <?php else: ?>
                            <p><?php esc_html_e('Sorry, no posts matched your criteria.', 'Sprinklr'); ?></p>
                        <?php endif; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>

                    <?php
                    foreach ($categories as $category) {
                        echo '<div class="tab-pane" id="' . $category->slug . '"><ul class="articles">';
                        $the_query = new WP_Query(array(
                            'post_type' => 'post',
                            'posts_per_page' => 12,
                            'category_name' => $category->slug
                        ));

                        while ($the_query->have_posts()) :
                            $the_query->the_post();
                            ?>

                            <li>
                                <article>

                                    <a href="<?php the_permalink(); ?>" rel="bookmark">

                                        <div class="post-thumbnail"><?php the_post_thumbnail('medium', array('class' => 'alignleft')); ?></div>

                                        <div class="post-content">
                                            <?php
                                             $before_cat = '<p class="post-category">';
                                             $categories_list = get_the_term_list ( get_the_id(), 'category', $before_cat , ', ', '</p>' );
                                             if ( $categories_list ){
                                                   printf( __( '%1$s', 'Sprinklr'),$categories_list );
                                             } 
                                            ?>
                                            <h2 class="entry-title"><?php the_title(); ?></h2>
                                            <p><?php echo substr(get_the_excerpt(), 0, 200); ?>[...]</p>
                                        </div>

                                        <div class="post-meta">
                                            <p><a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>" title="<?php the_author_meta('first_name'); ?> <br>
                                                  <?php the_author_meta('last_name'); ?>"><?php echo get_avatar(get_the_author_meta('ID'), 30); ?></a> 
                                                <?php the_time('n/j/y') ?> | <strong><?php echo round(sizeof(explode(" ", $data->post_content)) / 250) ?></strong> <?php esc_attr_e('Min Read', 'Sprinklr'); ?> | <strong><?php esc_attr_e('trending', 'Sprinklr'); ?></strong></p>
                                        </div>
                                    </a>
                                </article>
                            </li>



                            <?php
                        endwhile;
                        echo '</ul></div>';
                    }
                    ?>
                </div> 
            </div>
            <!-- END TABS -->


            <div class="col-md-1"></div>



        </div><!-- /.container --> 
    </div><!-- /.row --> 
</div>
<?php wp_reset_postdata(); ?>
<?php get_footer('blog'); ?>