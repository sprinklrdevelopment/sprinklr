</div>  <!-- fusion-row -->
</div>  <!-- #main -->

<?php
global $social_icons;

if (strpos(Avada()->settings->get('footer_special_effects'), 'footer_sticky') !== FALSE) {
    echo '</div>';
}

// Get the correct page ID
$c_pageID = Avada::c_pageID();

// Only include the footer
if (!is_page_template('blank.php')) {

    $footer_parallax_class = '';
    if (Avada()->settings->get('footer_special_effects') == 'footer_parallax_effect') {
        $footer_parallax_class = ' fusion-footer-parallax';
    }

    printf('<div class="fusion-footer%s">', $footer_parallax_class);

    // Check if the footer widget area should be displayed
    if (( Avada()->settings->get('footer_widgets') && get_post_meta($c_pageID, 'pyre_display_footer', true) != 'no' ) ||
            (!Avada()->settings->get('footer_widgets') && get_post_meta($c_pageID, 'pyre_display_footer', true) == 'yes' )
    ) {
        $footer_widget_area_center_class = '';
        if (Avada()->settings->get('footer_widgets_center_content')) {
            $footer_widget_area_center_class = ' fusion-footer-widget-area-center';
        }
        ?>
        <footer class="fusion-footer-widget-area fusion-widget-area<?php echo $footer_widget_area_center_class; ?>">
            <div class="fusion-row">
                <div class="fusion-columns fusion-columns-<?php echo Avada()->settings->get('footer_widgets_columns'); ?> fusion-widget-area">

                    <?php
                    // Check the column width based on the amount of columns chosen in Theme Options
                    $column_width = 12 / Avada()->settings->get('footer_widgets_columns');
                    if (Avada()->settings->get('footer_widgets_columns') == '5') {
                        $column_width = 2;
                    }

                    // Render as many widget columns as have been chosen in Theme Options
                    for ($i = 1; $i < 7; $i++) {
                        if (Avada()->settings->get('footer_widgets_columns') >= $i) {
                            if (Avada()->settings->get('footer_widgets_columns') == $i) {
                                echo sprintf('<div class="fusion-column fusion-column-last col-lg-%s col-md-%s col-sm-%s">', $column_width, $column_width, $column_width);
                            } else {
                                echo sprintf('<div class="fusion-column col-lg-%s col-md-%s col-sm-%s">', $column_width, $column_width, $column_width);
                            }

                            if (function_exists('dynamic_sidebar') &&
                                    dynamic_sidebar('avada-footer-widget-' . $i)
                            ) {
                                // All is good, dynamic_sidebar() already called the rendering
                            }
                            echo '</div>';
                        }
                    }
                    ?>

                    <div class="fusion-clearfix"></div>
                </div> <!-- fusion-columns -->
            </div> <!-- fusion-row -->
        </footer> <!-- fusion-footer-widget-area -->
        <?php
    } // end footer wigets check
    // Check if the footer copyright area should be displayed
    if (( Avada()->settings->get('footer_copyright') && get_post_meta($c_pageID, 'pyre_display_copyright', true) != 'no' ) ||
            (!Avada()->settings->get('footer_copyright') && get_post_meta($c_pageID, 'pyre_display_copyright', true) == 'yes' )
    ) {

        $footer_copyright_center_class = '';
        if (Avada()->settings->get('footer_copyright_center_content')) {
            $footer_copyright_center_class = ' fusion-footer-copyright-center';
        }
        ?>


        <footer class="site-footer">
            <div class="container">
                <div class="fusion-row">
                    <?php dynamic_sidebar('footer-logo-sidebar'); ?>
                    <div class="col-sm-5">
                        <?php dynamic_sidebar('footer-nav-menu'); ?>
                        <span><?php do_action('avada_footer_copyright_content'); ?></span>
                        <?php dynamic_sidebar('footer-nav-disclaimer'); ?>

                    </div>
                    <div class="footer-social-media">
                        <ul>
                            <?php
                            $facebook = Avada()->settings->get('facebook_link');
                            $twitter = Avada()->settings->get('twitter_link');
                            $youtube = Avada()->settings->get('youtube_link');
                            $linkedin = Avada()->settings->get('linkedin_link');
                            $apple = Avada()->settings->get('apple_link');
                            $android = Avada()->settings->get('android_link');
                            $flickr = Avada()->settings->get('flickr_link');
                            $rss = Avada()->settings->get('rss_link');
                            $vimeo = Avada()->settings->get('vimeo_link');
                            $instagram = Avada()->settings->get('instagram_link');
                            $pinterest = Avada()->settings->get('pinterest_link');
                            $tumblr = Avada()->settings->get('tumblr_link');
                            $google = Avada()->settings->get('google_link');
                            $dribbble = Avada()->settings->get('dribbble_link');
                            $digg = Avada()->settings->get('digg_link');
                            $blogger = Avada()->settings->get('blogger_link');
                            $skype = Avada()->settings->get('skype_link');
                            $forrst = Avada()->settings->get('forrst_link');
                            $myspace = Avada()->settings->get('myspace_link');
                            $deviantart = Avada()->settings->get('deviantart_link');
                            $yahoo = Avada()->settings->get('yahoo_link');
                            $reddit = Avada()->settings->get('reddit_link');
                            $paypal = Avada()->settings->get('paypal_link');
                            $dropbox = Avada()->settings->get('dropbox_link');
                            $soundcloud = Avada()->settings->get('soundcloud_link');
                            $vk = Avada()->settings->get('vk_link');
                            $email = Avada()->settings->get('email_link');
                            ?>
                            <?php if ($facebook != "") { ?><li class="facebook"><a target="_blank" href=<?php echo $facebook; ?>><i class="fa fa-facebook"></i></a></li><?php } ?>
                            <?php if ($twitter != "") { ?><li class="twitter"><a target="_blank" href="<?php echo $twitter; ?>"><i class="fa fa-twitter"></i></a></li><?php } ?>
                            <?php if ($youtube != "") { ?><li class="youtube-play"><a target="_blank" href="<?php echo $youtube; ?>"><i class="fa fa-youtube-play"></i></a></li><?php } ?>
                            <?php if ($linkedin != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $linkedin; ?>"><i class="fa fa-linkedin-square"></i></a></li><?php } ?>
                            <?php if ($flickr != "") { ?><li class="flickr"><a target="_blank" href="<?php echo $flickr; ?>"><i class="fa fa-flickr"></i></a></li><?php } ?>
                            <?php if ($rss != "") { ?><li class="rss"><a target="_blank" href="<?php echo $rss; ?>"><i class="fa fa-rss"></i></a></li><?php } ?>
                            <?php if ($vimeo != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $vimeo; ?>"><i class="fa fa-vimeo"></i></a></li><?php } ?>
                            <?php if ($instagram != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $instagram; ?>"><i class="fa fa-instagram"></i></a></li><?php } ?>
                            <?php if ($pinterest != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $pinterest; ?>"><i class="fa fa-pinterest"></i></a></li><?php } ?>
                            <?php if ($tumblr != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $tumblr; ?>"><i class="fa fa-tumblr"></i></a></li><?php } ?>
                            <?php if ($google != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $google; ?>"><i class="fa fa-google-plus"></i></a></li><?php } ?>
                            <?php if ($dribbble != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $dribbble; ?>"><i class="fa fa-dribbble"></i></a></li><?php } ?>
                            <?php if ($digg != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $digg; ?>"><i class="fa fa-digg"></i></a></li><?php } ?>
                            <?php if ($blogger != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $blogger; ?>"><i class="fa fa-wordpress"></i></a></li><?php } ?>
                            <?php if ($skype != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $skype; ?>"><i class="fa fa-skype"></i></a></li><?php } ?>
                            <?php if ($forrst != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $forrst; ?>"><i class="fa fa-forrst"></i></a></li><?php } ?>
                            <?php if ($myspace != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $myspace; ?>"><i class="fa fa-myspace"></i></a></li><?php } ?>
                            <?php if ($deviantart != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $deviantart; ?>"><i class="fa fa-deviantart"></i></a></li><?php } ?>
                            <?php if ($yahoo != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $yahoo; ?>"><i class="fa fa-yahoo"></i></a></li><?php } ?>
                            <?php if ($reddit != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $reddit; ?>"><i class="fa fa-reddit"></i></a></li><?php } ?>
                            <?php if ($paypal != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $paypal; ?>"><i class="fa fa-paypal"></i></a></li><?php } ?>
                            <?php if ($dropbox != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $dropbox; ?>"><i class="fa fa-dropbox"></i></a></li><?php } ?>
                            <?php if ($soundcloud != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $soundcloud; ?>"><i class="fa fa-soundcloud"></i></a></li><?php } ?>
                            <?php if ($vk != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $vk; ?>"><i class="fa fa-vk"></i></a></li><?php } ?>
                            <?php if ($email != "") { ?><li class="linkedin"><a target="_blank" href="<?php echo $email; ?>"><i class="fa fa-mail"></i></a></li><?php } ?>
                            <li class="border"></li>
                            <?php if ($apple != "") { ?><li class="apple"><a target="_blank" href="<?php echo $apple; ?>"><i class="fa fa-apple"></i></a></li><?php } ?>
                            <?php if ($android != "") { ?><li class="android"><a target="_blank" href="<?php echo $android; ?>"><i class="fa fa-android"></i></a></li><?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <?php
    } // end footer copyright area check
    ?>
    </div> <!-- fusion-footer -->
    <?php
} // end is not blank page check
?>
</div> <!-- wrapper -->

<?php
// Check if boxed side header layout is used; if so close the #boxed-wrapper container
if (( ( Avada()->settings->get('layout') == 'Boxed' && get_post_meta($c_pageID, 'pyre_page_bg_layout', true) == 'default' ) || get_post_meta($c_pageID, 'pyre_page_bg_layout', true) == 'boxed' ) &&
        Avada()->settings->get('header_position') != 'Top'
) {
    ?>
    </div> <!-- #boxed-wrapper -->
    <?php
}
?>

<a class="fusion-one-page-text-link fusion-page-load-link"></a>

<!-- W3TC-include-js-head -->

<?php
wp_footer();

// Echo the scripts added to the "before </body>" field in Theme Options
echo Avada()->settings->get('space_body');
?>

<!--[if lte IE 8]>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/respond.js"></script>
<![endif]-->
</body>
</html>
