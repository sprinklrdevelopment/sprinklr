<?php 
get_header('blog'); 
$facebook = Avada()->settings->get('facebook_link');
$twitter = Avada()->settings->get('twitter_link');
$linkedin = Avada()->settings->get('linkedin_link');
?>
<div id="progress"><div id="progressIndicator"></div></div>


<div id="Navbar">
    <div class="container">
        <div class="row">

            <?php include_once get_stylesheet_directory() . '/navbar.php'; ?>
        </div>
    </div>
</div>


<!-- BANNER -->
<div id="Banner">
    <div class="container">
        <div class="row">
            <?php if (have_posts()) : ?>
                <div class="banner" style="background:url(<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)) ?>)">
                </div>

            </div> 
        </div>
    </div>
    <!-- /BANNER -->




    <!-- CONTENT -->

    <div id="Content">   
        <div class="container">
            <div class="row category-<?php
            $category = get_the_category();
            echo $category[1]->category_nicename;
            ?>">
                <div class="col-md-1 fright"><!-- RIGHT SIDE SPACER --></div>

                <div class="col-md-1 social-media">
                    <div class="social">
                        <a class="addthis_button_facebook" data-original-title="Facebook"><img src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/assets/images/icon-facebook.png" alt="Facebook"></a>
                        <a class="addthis_button_twitter" data-original-title="twitter"><img src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/assets/images/icon-twitter.png" alt="Twitter"></a>
                        <a class="addthis_button_linkedin" data-original-title="linkedin"><img src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/assets/images/icon-linkedin.png" alt="LinkedIn"></a>
                    </div>
                </div>


                <!-- SIDEBAR -->
                <div class="col-md-3 center sidebar fright" id="Sidebar">

                    <?php dynamic_sidebar('spr-blog-sidebar'); ?>
                </div><!-- END SIDEBAR --> 


                <!-- ARTICLE -->
                <article>      
                    <?php while (have_posts()) : the_post(); ?>
                        <h1 class="entry-title">
                            <?php the_title(); ?>
                        </h1>

                        <div class="post-meta">
                            <p><?php the_time('l, F jS, Y') ?> | <strong>XX</strong> <?php esc_html_e('421', 'Sprinklr'); ?>min read | <strong><?php esc_html_e('you might also like', 'Sprinklr'); ?></strong> <?php esc_html_e('shares', 'Sprinklr'); ?> | <img src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/assets/images/trending.jpg"></p>
                        </div>

                        <?php the_content(); ?>

                        <p class="continue"><?php esc_html_e('Continue the conversation on', 'Sprinklr');?>&nbsp;&nbsp;&nbsp;<a href="<?php echo $facebook; ?>" target="_blank"><img src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/assets/images/icon-facebook.png" alt="Facebook"></a> <a href="<?php echo $twitter; ?>" target="_blank"><img src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/assets/images/icon-twitter.png" alt="Twitter"></a> <a href="<?php echo $linkedin; ?>" target="_blank"><img src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/assets/images/icon-linkedin.png" alt="LinkedIn"></a></p>
                    <?php endwhile; ?>
                <?php endif; ?>
            </article>



        </div><!-- /container --> 
    </div><!-- /row --> 
</div><!-- /Content -->


<!-- SUBSCRIBE -->
<div class="band bkg-green">
    <div class="container">
        <div class="row">

            <div class="col-md-1"></div>
            <div class="col-md-6"><h6><?php esc_html_e('Get access to industry-leading social content', 'Sprinklr');?></h6></div>

            <div class="col-md-4 subscribe">
                <form><input type="email" placeholder="<?php esc_html_e('Enter Your Email', 'Sprinklr');?>"><button class="bkg-yellow"><?php esc_html_e('Subscribe', 'Sprinklr');?></button></form>
            </div><!-- /.subscribe -->

            <div class="col-md-1"></div>

        </div>
    </div>
</div><!-- /.band -->





<div id="Related" class="bkg-grey">
    <div class="container">
        <div class="row">

            <div class="col-md-1">&nbsp;</div>

            <div class="col-md-7 related">
                <h5><strong><?php esc_html_e('you\'ll probably also like', 'Sprinklr');?>...</strong></h5>

                <ul>
                    <?php
                    $orig_post = $post;
                    global $post;
                    $categories = get_the_category($post->ID);
                    $category_ids = array();
                    if ($categories) {
                    
                        foreach($categories as $individual_category){
                            $category_ids[] = $individual_category->term_id;
                        }
                    }
                    $args=array(
                    'post__not_in' => array($post->ID),
                    'posts_per_page'=> 3, // Number of related posts that will be shown.
                    );
                    if(count($category_ids)>0){
                        $args['category__in'] = $category_ids;
                    }
                    
                    $my_query = new wp_query( $args );
                    if( $my_query->have_posts() ):
                    
                    ?>

                   
                    <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
                        <li>
                            <div class="post-thumbnail col-md-5"><?php the_post_thumbnail('large', array('class' => 'alignleft')); ?></div>

                            <div class="excerpt col-md-6">
                                <p class="post-category"><?php
                                    $category = get_the_category();
                                    echo $category[0]->cat_name;
                                    ?></p>
                                <div class="entry-title">
                                    <h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
                                </div>


                                <section id="Meta">

                                    <div class="post-meta"><p><a href="#" title="<?php the_author_meta('first_name'); ?> <?php the_author_meta('last_name'); ?>"><?php echo get_avatar(get_the_author_meta('ID'), 40); ?></a> <strong>15</strong> min read | <strong>421</strong> Shares</p></div>
                                </section>   
                            </div>
                        </li>
                    <?php endwhile; ?>
                    <?php endif; ?>



                </ul>
            </div>

            <div class="col-md-1 fright">&nbsp;</div>


            <div class="col-md-3 recent fright">
                <h5><strong><?php esc_html_e('recently published', 'Sprinklr');?></strong></h5>
                <ul>
                    <?php query_posts('post_status=publish&order=DESC&posts_per_page=3'); ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <li>            
                            <p class="post-category"><?php
                                $category = get_the_category();
                                echo $category[0]->cat_name;
                                ?></p>
                            <div class="entry-title">
                                <h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
                            </div>


                            <section id="Meta">
                                <div class="post-meta"><p><strong>15</strong> min read | <strong>421</strong> Shares</p></div>
                            </section>   
                        </li>
                    <?php endwhile; ?>
                </ul>   
            </div>    


        </div>
    </div>
</div>
<script>



    progressMonitor = function () {
        var $ = jQuery,
                indicator = $('#progressIndicator'),
                content = $('#Content'),
                height = $(content).height() - 500;


        jQuery(document).on('scroll', function () {
            $(indicator).css('width', ($(document).scrollTop() / height) * 100 + '%');
        });

    }

    defer(progressMonitor);

    function defer(method) {
        if (window.jQuery)
            method()
        else
            setTimeout(function () {
                defer(method)
            }, 50);
    }



</script>
<!-- SCROLL THEN FIXED ITEMS (SOCIAL MEDIA AND MENU) -->    
<script>
    var main = function () {
        var menu = $('.fixnav')

        $(document).scroll(function () {
            if ($(this).scrollTop() >= 110) {
                menu.addClass('top')
            } else {
                menu.removeClass('top')
            }
        })
    }
    $(document).ready(main);
    var main = function () {
        var menu = $('.social-media')

        $(document).scroll(function () {
            if ($(this).scrollTop() >= 580) {
                menu.addClass('top')
            } else {
                menu.removeClass('top')
            }
        })
    }
    $(document).ready(main);
</script>
<?php get_footer('blog'); ?>