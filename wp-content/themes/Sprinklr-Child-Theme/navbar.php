<div class="navbar fixnav" >
    <div class="col-md-1"></div>


    <div class="col-md-3">
        <form class="subscribe">
            <input type="email" placeholder="<?php esc_html_e('Enter Your Email', 'Sprinklr');?>"><button class="bkg-blue"><?php esc_html_e('Subscribe', 'Sprinklr');?></button>
        </form>
    </div><!-- /.subscribe -->


    <!-- CATEGORY TABS -- THIS CODE IS FULLY FUNCTIONAL, THOUGH I'M SURE THERE'S A CLEANER WAY TO CODE THE PHP HERE -->
    <div>   
        <nav class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="category-4"><a href="/category/blog/"><?php esc_html_e('All', 'Sprinklr');?></a>
                    <?php
                    global $myCat;
                    $myCat = 25;
                    ?> 
                </li>

                <li class="category-8"><a href="/category/blog/managing-customer-experiences/"><?php esc_html_e('Customer Experience', 'Sprinklr');?></a>
                    <?php
                    $myCat = 29;
                    include get_stylesheet_directory() . '/inc/carousel.php';
                    ?> 
                </li>

                <li class="category-42"><a href="/category/blog/advertising/"><?php esc_html_e('Advertising', 'Sprinklr');?></a>
                    <?php
                    $myCat = '42';
                    include get_stylesheet_directory() . '/inc/carousel.php';
                    ?> 
                </li>

                <li class="category-5"><a href="/category/blog/marketing/"><?php esc_html_e('Marketing', 'Sprinklr');?></a>
                    <?php
                    $myCat = 26;
                    include get_stylesheet_directory() . '/inc/carousel.php';
                    ?> 
                </li>

                <li class="category-33"><a href="/category/blog/social-data/"><?php esc_html_e('Social Data', 'Sprinklr');?></a>
                    <?php
                    $myCat = '33';
                    include get_stylesheet_directory() . '/inc/carousel.php';
                    ?> 
                </li>

                <li class="category-43"><a href="/category/blog/leadership/"><?php esc_html_e('Leadership', 'Sprinklr');?></a>
                    <?php
                    $myCat = '43';
                    include get_stylesheet_directory() . '/inc/carousel.php';
                    ?> 
                </li>

                <li class="category-47"><a href="/category/blog/leadership/"><?php esc_html_e('Customer Care', 'Sprinklr');?></a>
                    <?php
                    $myCat = '47';
                    include get_stylesheet_directory() . '/inc/carousel.php';
                    ?> 
                </li>
            </ul> 
            
    </div>


    <?php
    include_once get_stylesheet_directory() . '/inc/search-bar.php';
    ?>
    <?php wp_reset_postdata(); ?>