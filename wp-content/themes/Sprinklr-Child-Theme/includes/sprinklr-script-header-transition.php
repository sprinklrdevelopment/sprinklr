<?php
/**
 *
 * Set the main menu color to 'White' when Header background is transparent.
 * Then swtich back to default color (#333333) of menu, when Sticky header is ON.
 */
add_action('wp_footer', 'sprinklr_header_menu_color_transition');
function sprinklr_header_menu_color_transition() {
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {

            if ($('.fusion-header-wrapper').css('position') === 'absolute') {
                var lastScrollTop = 0;
                $('.fusion-main-menu > ul > li > a').css('color', '#fff');
                $('.fusion-standard-logo').attr('src', '<?php echo get_stylesheet_directory_uri(); ?>/assets/images/SprinklrLogo-Main-White-90x42.png');

                $(window).scroll(function () {
                    var st = $(this).scrollTop();
                    if (st > lastScrollTop) {
                        // Condition will be checked when scroll DOWN
                        if ($('.fusion-header-wrapper').hasClass('fusion-is-sticky')) {
                            $('.fusion-main-menu > ul > li > a').css('color', '#333333');
                            $('.fusion-main-menu-icon').css('color','#333333');
                        }
                    } else {
                        // Condition will be checked when scroll UP
                        if (st == 0)
                        {
                            $('.fusion-main-menu > ul > li > a').css('color', '#fff');
                        }
                        else {
                            $('.fusion-main-menu > ul > li > a').css('color', '#333333');
                            $('.fusion-main-menu-icon').css('color','#333333');
                        }
                    }
                    lastScrollTop = st;
                });
            }
        });
    </script>
    <?php
}
