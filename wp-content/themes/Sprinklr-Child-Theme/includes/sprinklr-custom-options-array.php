<?php

/**
 * Overriding Options Array of AVADA
 * 
 * @global $of_options
 * @return the addtional icons URL fields of Apple & Android 
 */
function of_options() {
    global $of_options;

    $social_links[] = array("name" => __("Facebook", "Avada"),
        "desc" => __("Insert your custom link to show the Facebook icon. Leave blank to hide icon.", "Avada"),
        "id" => "facebook_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Flickr", "Avada"),
        "desc" => __("Insert your custom link to show the Flickr icon. Leave blank to hide icon.", "Avada"),
        "id" => "flickr_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("RSS", "Avada"),
        "desc" => __("Insert your custom link to show the RSS icon. Leave blank to hide icon.", "Avada"),
        "id" => "rss_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Twitter", "Avada"),
        "desc" => __("Insert your custom link to show the Twitter icon. Leave blank to hide icon.", "Avada"),
        "id" => "twitter_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Vimeo", "Avada"),
        "desc" => __("Insert your custom link to show the Vimeo icon. Leave blank to hide icon.", "Avada"),
        "id" => "vimeo_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Youtube", "Avada"),
        "desc" => __("Insert your custom link to show the Youtube icon. Leave blank to hide icon.", "Avada"),
        "id" => "youtube_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Instagram", "Avada"),
        "desc" => __("Insert your custom link to show the Instagram icon. Leave blank to hide icon.", "Avada"),
        "id" => "instagram_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Pinterest", "Avada"),
        "desc" => __("Insert your custom link to show the Pinterest icon. Leave blank to hide icon.", "Avada"),
        "id" => "pinterest_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Tumblr", "Avada"),
        "desc" => __("Insert your custom link to show the Tumblr icon. Leave blank to hide icon.", "Avada"),
        "id" => "tumblr_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Google+", "Avada"),
        "desc" => __("Insert your custom link to show the Google+ icon. Leave blank to hide icon.", "Avada"),
        "id" => "google_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Dribbble", "Avada"),
        "desc" => __("Insert your custom link to show the Dribbble icon. Leave blank to hide icon.", "Avada"),
        "id" => "dribbble_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Digg", "Avada"),
        "desc" => __("Insert your custom link to show the Digg icon. Leave blank to hide icon.", "Avada"),
        "id" => "digg_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("LinkedIn", "Avada"),
        "desc" => __("Insert your custom link to show the LinkedIn icon. Leave blank to hide icon.", "Avada"),
        "id" => "linkedin_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Blogger", "Avada"),
        "desc" => __("Insert your custom link to show the Blogger icon. Leave blank to hide icon.", "Avada"),
        "id" => "blogger_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Skype", "Avada"),
        "desc" => __("Insert your custom link to show the Skype icon. Leave blank to hide icon.", "Avada"),
        "id" => "skype_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Forrst", "Avada"),
        "desc" => __("Insert your custom link to show the Forrst icon. Leave blank to hide icon.", "Avada"),
        "id" => "forrst_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Myspace", "Avada"),
        "desc" => __("Insert your custom link to show the Myspace icon. Leave blank to hide icon.", "Avada"),
        "id" => "myspace_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Deviantart", "Avada"),
        "desc" => __("Insert your custom link to show the Deviantart icon. Leave blank to hide icon.", "Avada"),
        "id" => "deviantart_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Yahoo", "Avada"),
        "desc" => __("Insert your custom link to show the Yahoo icon. Leave blank to hide icon.", "Avada"),
        "id" => "yahoo_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Reddit", "Avada"),
        "desc" => __("Insert your custom link to show the Reddit icon. Leave blank to hide icon.", "Avada"),
        "id" => "reddit_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Paypal", "Avada"),
        "desc" => __("Insert your custom link to show the Paypal icon. Leave blank to hide icon.", "Avada"),
        "id" => "paypal_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Dropbox", "Avada"),
        "desc" => __("Insert your custom link to show the Dropbox icon. Leave blank to hide icon.", "Avada"),
        "id" => "dropbox_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Soundcloud", "Avada"),
        "desc" => __("Insert your custom link to show the Soundcloud icon. Leave blank to hide icon.", "Avada"),
        "id" => "soundcloud_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("VK", "Avada"),
        "desc" => __("Insert your custom link to show the VK icon. Leave blank to hide icon.", "Avada"),
        "id" => "vk_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Email Address", "Avada"),
        "desc" => __("Insert your custom link to show the mail icon. Leave blank to hide icon.", "Avada"),
        "id" => "email_link",
        "std" => "",
        "type" => "text");

    $social_links[] = array("name" => __("Android", "Avada"),
        "desc" => __("Insert your custom link to show the mail icon. Leave blank to hide icon.", "Avada"),
        "id" => "android_link",
        "std" => "",
        "type" => "text");
    $social_links[] = array("name" => __("Apple Link", "Avada"),
        "desc" => __("Insert your custom link to show the mail icon. Leave blank to hide icon.", "Avada"),
        "id" => "apple_link",
        "std" => "",
        "type" => "text");
    $of_options = of_options_array();
    $key = array_search('social_sorter', array_column($of_options, 'id'));
    $of_options[$key] = array("name" => "",
        "desc" => "",
        "id" => "social_sorter",
        "std" => "",
        "type" => "fusion_sorter",
        "fields" => $social_links,
    );
}
