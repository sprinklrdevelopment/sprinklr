<?php

/**
 * Enqueue scripts and styles of Bootstrap
 */
add_action('wp_enqueue_scripts', 'sprinklr_bootstrap_scripts');

function sprinklr_bootstrap_scripts() {

    /* load bootstrap style */
    wp_enqueue_style('bootstrap-min', get_stylesheet_directory_uri() . '/assets/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap-theme', get_stylesheet_directory_uri() . '/assets/css/bootstrap-theme.css');
    wp_enqueue_style('font-awesome', get_stylesheet_directory_uri() . '/assets/css/font-awesome.min.css');

    wp_enqueue_script('bootstrap-min-js', get_stylesheet_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), '', TRUE);
}

/**
 * Add Script for Header Menu color transition 
 */
include_once ( get_stylesheet_directory() . '/includes/sprinklr-script-header-transition.php' );

/**
 * Filters to remove the style and scripts versions
 *
 * @return Scripts & Styles without Versions ( eg without => ?ver=1.1 )
 * */
add_filter('script_loader_src', 'sprinklr_remove_scripts_version', 15, 1);
add_filter('style_loader_src', 'sprinklr_remove_scripts_version', 15, 1);
function sprinklr_remove_scripts_version($src) {
    $parts = explode('?ver', $src);
    return $parts[0];
}

/**
 * Register widgets area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
add_action('widgets_init', 'sprinklr_widgets_init');
function sprinklr_widgets_init() {

    $html = '<a href="' . home_url() . '" class="footer-logo">';
    register_sidebar(array(
        'name' => __('Footer Logo', 'Sprinklr'),
        'id' => 'footer-logo-sidebar',
        'before_widget' => $html,
        'after_widget' => '</a>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));
    register_sidebar(array(
        'name' => __('Footer Menu', 'Sprinklr'),
        'id' => 'footer-nav-menu',
        'before_widget' => '<div class="footer-menu">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));
    register_sidebar(array(
        'name' => __('Footer Disclaimer', 'Sprinklr'),
        'id' => 'footer-nav-disclaimer',
        'before_widget' => '<div class="disclaimers">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));
    register_sidebar( array(
            'name' => __( 'Blog Detail Sidebar', 'Sprinklr' ),
            'id' => 'blog-sidebar',
            'description' => __( 'Appears when using the optional  Page template with a page set as Static Front Page', 'Sprinklr' ),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
    ) );
}

/**
 * Output the footer copyright notice
 *
 * @return void directly echos the footer copyright notice HTML markup
 * */
add_action('avada_footer_copyright_content', 'avada_render_footer_copyright_notice', 10);
function avada_render_footer_copyright_notice() {

    echo do_shortcode(Avada()->settings->get('footer_text'));
}

/**
 * Removes the Avada footer Social Icons
 *
 * @return Remove the current using Avada footer Social Icons
 * */
add_action('avada_footer_copyright_content', 'remove_avada_social_icons');
function remove_avada_social_icons() {
    remove_action('avada_footer_copyright_content', 'avada_render_footer_social_icons', 15);
}

/**
 * Add Custom Options Array ( Fields values using in Theme Options )
 */
include_once ( get_stylesheet_directory() . '/includes/sprinklr-custom-options-array.php' );

