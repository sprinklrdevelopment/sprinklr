<?php
/**
 * @Recent posts widget Class
 *
 *
 */
if (!class_exists('sprinklr_recentposts')) {

    class sprinklr_recentposts extends WP_Widget {
        /**
         * Outputs the content of the widget
         *
         * @param array $args
         * @param array $instance
         */

        /**
         * @init Recent posts Module
         *
         *
         */
        
        function __construct() {
            $widget_ops = array('classname' => 'widget-recent-blog widget_latest_post', 'description' => 'Recent Posts from category.');
            parent::__construct('sprinklr_recentposts', 'Sprinklr Recent Posts', $widget_ops);
        }

        /**
         * @Recent posts html form
         *
         *
         */
        function form($instance) {
            $instance = wp_parse_args((array) $instance, array('title' => ''));
            $title = $instance['title'];
            $select_category = isset($instance['select_category']) ? esc_attr($instance['select_category']) : '';
            $showcount = isset($instance['showcount']) ? esc_attr($instance['showcount']) : '';
            $thumb = isset($instance['thumb']) ? esc_attr($instance['thumb']) : '';
            ?>
            <p>
                <label for="<?php echo sprinklr_allow_special_char($this->get_field_id('title')); ?>"> Title:
                    <input class="upcoming" id="<?php echo sprinklr_allow_special_char($this->get_field_id('title')); ?>" size="40" name="<?php echo sprinklr_allow_special_char($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
                </label>
            </p>
            <p>
                <label for="<?php echo sprinklr_allow_special_char($this->get_field_id('select_category')); ?>"> Select Category:
                    <select id="<?php echo sprinklr_allow_special_char($this->get_field_id('select_category')); ?>" name="<?php echo sprinklr_allow_special_char($this->get_field_name('select_category')); ?>" style="width:225px">
                        <option value="" >All</option>
                        <?php
                        $categories = get_categories();
                        if ($categories <> "") {
                            foreach ($categories as $category) {
                                ?>
                                <option <?php
                                if ($select_category == $category->slug) {
                                    echo 'selected';
                                }
                                ?> value="<?php echo sprinklr_allow_special_char($category->slug); ?>" ><?php echo sprinklr_allow_special_char($category->name); ?></option>
                                    <?php
                                }
                            }
                            ?>
                    </select>
                </label>
            </p>
            <p>
                <label for="<?php echo sprinklr_allow_special_char($this->get_field_id('showcount')); ?>"> Number of Posts To Display:
                    <input class="upcoming" id="<?php echo sprinklr_allow_special_char($this->get_field_id('showcount')); ?>" size='2' name="<?php echo sprinklr_allow_special_char($this->get_field_name('showcount')); ?>" type="text" value="<?php echo esc_attr($showcount); ?>" />
                </label>
            </p>
           
            <?php
        }

        /**
         * @Recent posts update form data
         *
         *
         */
        function update($new_instance, $old_instance) {
            $instance = $old_instance;
            $instance['title'] = $new_instance['title'];
            $instance['select_category'] = $new_instance['select_category'];
            $instance['showcount'] = $new_instance['showcount'];
            $instance['thumb'] = $new_instance['thumb'];

            return $instance;
        }

        /**
         * @Display Recent posts widget
         *
         *
         */
        function widget($args, $instance) {
            global $cs_node;

            extract($args, EXTR_SKIP);
            $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
            $select_category = empty($instance['select_category']) ? ' ' : apply_filters('widget_title', $instance['select_category']);
            $showcount = empty($instance['showcount']) ? ' ' : apply_filters('widget_title', $instance['showcount']);
            if ($instance['showcount'] == "") {
                $instance['showcount'] = '4';
            }

            echo sprinklr_allow_special_char($before_widget);

            if (!empty($title) && $title <> ' ') {
                echo sprinklr_allow_special_char($before_title);
                echo sprinklr_allow_special_char($title);
                echo sprinklr_allow_special_char($after_title);
            }

            global $post;
            ?>
            <?php
            
            /**
             * @Display Recent posts
             *
             *
             */
            if (isset($select_category) and $select_category <> ' ' and $select_category <> '') {
                $args = array('posts_per_page' => "$showcount", 'order' => 'DESC', 'post_type' => 'post', 'category_name' => "$select_category", 'ignore_sticky_posts' => 1);
            } else {
                $args = array('posts_per_page' => "$showcount", 'order' => 'DESC', 'post_type' => 'post', 'ignore_sticky_posts' => 1);
            }

            $custom_query = new WP_Query($args);
            if ($custom_query->have_posts() <> "") {

                echo '<div class="related"><ul>';
                while ($custom_query->have_posts()) : $custom_query->the_post();
                    ?>
                    <li>
                        <div class="post-thumbnail"><?php the_post_thumbnail('large', array('class' => 'alignleft')); ?></div>

                        <div class="entry">
                            <p class="post-category"><?php
                                $category = get_the_category();
                                echo $category[0]->cat_name;
                                ?></p>
                            <div class="entry-title">
                                <h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
                            </div>
                    </li>
                    <?php
                endwhile;


                echo '</ul></div>';
            }
            else {
                ?>
                    <p><?php esc_html_e('Sorry, no posts matched your criteria.', 'Sprinklr'); ?></p>
                <?php
            }
            echo sprinklr_allow_special_char($after_widget);
        }

    }

}
add_action('widgets_init', create_function('', 'return register_widget("sprinklr_recentposts");'));



/**
 * @Author Detail widget Class
 *
 *
 */
if (!class_exists('sprinklr_author_detail')) {

    class sprinklr_author_detail extends WP_Widget {
        /**
         * Outputs the content of the widget
         *
         * @param array $args
         * @param array $instance
         */

        /**
         * @init Author Info Module
         *
         *
         */
        
        function __construct() {
            $widget_ops = array('classname' => 'widget-recent-blog widget_latest_post', 'description' => 'Author Information.');
            parent::__construct('sprinklr_author_detail', 'Sprinklr Auhtor Info', $widget_ops);
        }

        /**
         * @Author Info html form
         *
         *
         */
        function form($instance) {
            $instance = wp_parse_args((array) $instance, array('title' => ''));
            $title = $instance['title'];
            ?>
            <p>
                <label for="<?php echo sprinklr_allow_special_char($this->get_field_id('title')); ?>"> Title:
                    <input class="upcoming" id="<?php echo sprinklr_allow_special_char($this->get_field_id('title')); ?>" size="40" name="<?php echo sprinklr_allow_special_char($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
                </label>
            </p>        
            <?php
        }

        /**
         * @Recent posts update form data
         *
         *
         */
        function update($new_instance, $old_instance) {
            $instance = $old_instance;
            $instance['title'] = $new_instance['title'];
            return $instance;
        }

        /**
         * @Display Author Info widget
         *
         *
         */
        function widget($args, $instance) {
            global $cs_node;

            extract($args, EXTR_SKIP);
            $title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
            
            echo sprinklr_allow_special_char($before_widget);

            if (!empty($title) && $title <> ' ') {
                echo sprinklr_allow_special_char($before_title);
                echo sprinklr_allow_special_char($title);
                echo sprinklr_allow_special_char($after_title);
            }

            global $post;
            ?>
            <?php
            
            /**
             * @Display Author Info
             *
             *
             */?>
            <div class="post-meta">
                        <?php echo get_avatar(get_the_author_meta('ID'), 175); ?>
                        <p>by <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"><?php the_author_meta('first_name'); ?> <?php the_author_meta('last_name'); ?></a><br>
                            <?php $twitterHandle = get_the_author_meta('twitter'); ?> <a href="http://www.twitter.com/<?php echo $twitterHandle; ?>" target="_blank"><?php echo $twitterHandle; ?></a></p>
            </div><?php
            
            echo sprinklr_allow_special_char($after_widget);
        }

    }

}
add_action('widgets_init', create_function('', 'return register_widget("sprinklr_author_detail");'));
