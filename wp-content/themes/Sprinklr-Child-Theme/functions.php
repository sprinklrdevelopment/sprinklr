<?php

/**
 * Sprinklr Custom functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package Avada
 */
add_action('after_setup_theme', 'sprinklr_child_theme_setup');

function sprinklr_child_theme_setup() {

    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on Avada, use a find and replace
     * to change 'Avada' to the name of your theme in all the template files
     */
    $lang = get_stylesheet_directory() . '/languages';
    load_child_theme_textdomain('Sprinklr', $lang);
    set_post_thumbnail_size(624, 9999); // Unlimited height, soft crop
    add_image_size('banner', 900, 9999, true);
    add_image_size('medium', 400, 300, true);
}

/**
 * Enqueue Parent Style
 */
add_action('wp_enqueue_scripts', 'theme_enqueue_styles', 12);

function theme_enqueue_styles() {
    wp_enqueue_style('avada-parent-stylesheet', get_template_directory_uri() . '/style.css');

    if (sprinklr_is_blog()) {
        wp_enqueue_style('blog-stylesheet', get_stylesheet_directory_uri() . '/assets/css/blog.css');
        wp_enqueue_style('pagination-stylesheet', get_stylesheet_directory_uri() . '/assets/css/pagination.css');
    }
    if(is_single()){
       wp_enqueue_script( 'cs_addthis', 'http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56bdf5f79b26c2a9', '', '', true);
    }
}

function sprinklr_is_blog() {
    return ( is_archive() || is_author() || is_search() || is_category() || is_home() || is_single() || is_tag()) && 'post' == get_post_type();
}

/**
 * Add Sprinklr custom functions for theme customization 
 */
include_once ( get_stylesheet_directory() . '/includes/sprinklr-custom-functions.php' );

include_once ( get_stylesheet_directory() . '/includes/widgets.php' );

/**
 * Defer scripts
 */
if (!function_exists('sprinklr_defer_parsing_of_js')) {

    function sprinklr_defer_parsing_of_js($url) {
        if (FALSE === strpos($url, '.js'))
            return $url;
        if (strpos($url, 'jquery.js'))
            return $url;
        return "$url' defer ";
    }

    //add_filter( 'clean_url', 'sprinklr_defer_parsing_of_js', 11, 1 );
}

// grab token
function sprinklr_getToken($code) {
    $url = 'https://api2.sprinklr.com/prod0/oauth/token?grant_type=authorization_code';
    $data = array('client_id' => 'zqc5zu4qrwzz6uagq3gmtw3r', 'client_secret' => 'rP3v5hj7FgaFgqFFec9NDqrnVDSqjjTnUk8eKBJdpgSDb4qVjgukku9mdrp5AxaG', 'redirect_uri' => 'https://sprinklr.com', 'code' => $code);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    $result = curl_exec($ch);
    print $result;
    curl_close($ch);
    exit();
}

function sprinklr_getDashboard($key, $dashboard, $column) {
    $url = 'https://api2.sprinklr.com/prod0/api/v1/dashboard/' . str_replace(" ", "%20", $dashboard) . '/stream/' . str_replace(" ", "%20", $column);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $key, 'Key: zqc5zu4qrwzz6uagq3gmtw3r'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    print $result;

    curl_close($ch);
    exit();
}

function sprinklr_getFeed($key, $feedId) {
    $url = 'https://api2.sprinklr.com/prod0/api/v1/stream/' . $feedId . '/feed?rows=20&clientId=4';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $key, 'Key: zqc5zu4qrwzz6uagq3gmtw3r'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = json_decode(curl_exec($ch));
    curl_close($ch);
    return $result;
}

if (false === ( $value = get_transient('sprinklr_feed') )) {
    delete_transient('sprinklr_feed');
    $feed = array_merge(sprinklr_getFeed($key, "5592dc16e4b025ad2906e976"), sprinklr_getFeed($key, "5592dc16e4b025ad2906e96a"));
    set_transient('sprinklr_feed', $feed, 12 * HOUR_IN_SECONDS);
}

/** MY GALLERY LIGHTBOX * */
function nggGetGallery($galleryID, $template = '', $images = false) {

    global $nggRewrite;

    $ngg_options = nggGallery::get_option('ngg_options');

    //Set sort order value, if not used (upgrade issue)
    $ngg_options['galSort'] = ($ngg_options['galSort']) ? $ngg_options['galSort'] : 'pid';
    $ngg_options['galSortDir'] = ($ngg_options['galSortDir'] == 'DESC') ? 'DESC' : 'ASC';

    // get gallery values
    //TODO: Use pagination limits here to reduce memory needs
    $picturelist = nggdb::get_gallery($galleryID, $ngg_options['galSort'], $ngg_options['galSortDir']);
    return $picturelist;
}

add_filter('posts_where', 'no_privates');

function no_privates($where) {
    if (is_admin())
        return $where;

    global $wpdb;
    return " $where AND {$wpdb->posts}.post_status != 'private' ";
}

// BELOW IS ALL THE CONTENT YOU REALLY NEED FROM THIS FILE 


function get_post_data($postId) {
    global $wpdb;
    return $wpdb->get_row("SELECT * FROM $wpdb->posts WHERE ID=$postId");
}

// SHOW AUTHOR AVATARS IN LIST
function contributors() {
    global $wpdb;

    $authors = $wpdb->get_results("SELECT ID, user_nicename from $wpdb->users ORDER BY display_name");

    foreach ($authors as $author) {
        echo "<li>";
        echo "<a href=\"" . get_bloginfo('url') . "/?author=";
        echo $author->ID;
        echo "\">";
        echo get_avatar($author->ID);
        echo "</a>";
        echo "</li>";
    }
}

function sprinklr_custom_pagination($numpages = '', $pagerange = '', $paged = '') {

    if (empty($pagerange)) {
        $pagerange = 2;
    }

    /**
     * This first part of our function is a fallback
     * for custom pagination inside a regular loop that
     * uses the global $paged and global $wp_query variables.
     * 
     * It's good because we can now override default pagination
     * in our theme, and use this function in default quries
     * and custom queries.
     */
    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }
    if ($numpages == '') {
        global $wp_query;
        $numpages = $wp_query->max_num_pages;
        if (!$numpages) {
            $numpages = 1;
        }
    }

    /**
     * We construct the pagination arguments to enter into our paginate_links
     * function. 
     */
    $pagination_args = array(
        'base' => get_pagenum_link(1) . '%_%',
        'format' => 'page/%#%',
        'total' => $numpages,
        'current' => $paged,
        'show_all' => False,
        'end_size' => 1,
        'mid_size' => $pagerange,
        'prev_next' => True,
        'prev_text' => __('&laquo;'),
        'next_text' => __('&raquo;'),
        'type' => 'plain',
        'add_args' => false,
        'add_fragment' => ''
    );

    $paginate_links = paginate_links($pagination_args);

    if ($paginate_links) {
        echo "<nav class='custom-pagination'>";
        echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
        echo $paginate_links;
        echo "</nav>";
    }
}


function sprinklr_allow_special_char($string=''){
    return $string;
}