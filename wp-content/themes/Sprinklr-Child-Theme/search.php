<?php
/* Template Name:  Search */
get_header('blog');
?>

<div id="Navbar">
    <div class="container">
      <div class="row">
      <?php include_once get_stylesheet_directory() . '/navbar.php'; ?>
      </div>
    </div>
</div>

<!-- CONTENT -->
<div id="Content"> 
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <ul class="articles">
                    <?php
                    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                    if (have_posts()) : while (have_posts()) : the_post();
                            $data = get_post_data(get_the_id());
                            ?>

                            <li>
                                <article>

                                    <a href="<?php the_permalink(); ?>" rel="bookmark">

                                        <div class="post-thumbnail"><?php the_post_thumbnail('medium', array('class' => 'alignleft')); ?></div>

                                        <div class="post-content">
                                            
                                            <?php
                                             $before_cat = '<p class="post-category">';
                                             $categories_list = get_the_term_list ( get_the_id(), 'category', $before_cat , ', ', '</p>' );
                                             if ( $categories_list ){
                                                   printf( __( '%1$s', 'Sprinklr'),$categories_list );
                                             } 
                                            ?>
                                            
                                            <h2 class="entry-title"><?php the_title(); ?></h2>
                                            <p><?php echo substr(get_the_excerpt(), 0, 200); ?>[...]</p>
                                        </div>

                                        <div class="post-meta">
                                            <p><a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>" title="<?php the_author_meta('first_name'); ?> <br>
                                                  <?php the_author_meta('last_name'); ?>"><?php echo get_avatar(get_the_author_meta('ID'), 30); ?></a> 
                                                <?php the_time('n/j/y') ?> | <strong><?php echo round(sizeof(explode(" ", $data->post_content)) / 250) ?></strong> Min Read | <strong>XX</strong> Shares</p>
                                        </div>
                                    </a>
                                </article>
                            </li>

                        <?php endwhile; ?>
                        <?php
                        if (function_exists('sprinklr_custom_pagination')) {
                            global $wp_query;
                            sprinklr_custom_pagination($wp_query->max_num_pages, "", $paged);
                        }
                        ?>
                    <?php else: ?>
                        <p><?php esc_html_e('Sorry, no posts matched your criteria.', 'Sprinklr'); ?></p>
                    <?php endif; ?>
                    <?php wp_reset_postdata(); ?>


                </ul>
            </div><!-- /ARTICLES -->


            <div class="col-md-1"></div>


        </div><!-- /.container --> 
    </div><!-- /.row --> 
</div>
<?php get_footer(); ?>