<?php 
global $myCat;
?>
<ul class="carousel-menu">

    <div id="myCarousel-<?php echo $myCat ?>" class="carousel slide">
        <div class="carousel-inner">

            <!-- SLIDE 1 --> 


            <div class="item active">

                <?php
                $the_query = new WP_Query(array(
                    'cat' => $myCat,
                    'posts_per_page' => 4
                ));
                while ($the_query->have_posts()) : $the_query->the_post();
                    ?>

                    <li>
                        <div class="post-thumbnail bkg-ltgrey"><?php the_post_thumbnail('medium', array('class' => 'alignleft')); ?></div>
                        <a href="<?php the_permalink(); ?>" style="line-height:21px; padding:0; display:block; background-color:transparent;"><br>
                            <?php the_title(); ?></a>       
                    </li>

                <?php endwhile;
                wp_reset_postdata();
                ?>
            </div><!-- item active -->


            <div class="item">
                <?php
                $the_query = new WP_Query(array(
                    'cat' => $myCat,
                    'posts_per_page' => 4,
                    'offset' => 4
                ));
                while ($the_query->have_posts()) : $the_query->the_post();
                    ?>
                    <li>
                        <div class="post-thumbnail bkg-ltgrey"><?php the_post_thumbnail('medium', array('class' => 'alignleft')); ?></div>
                        <a href="<?php the_permalink(); ?>" style="line-height:21px; padding:0; display:block; background-color:transparent;"><br>
                    <?php the_title(); ?></a>       
                    </li>
                <?php endwhile;
                wp_reset_postdata();
                ?>

            </div><!-- item -->
            <div class="item">
                <?php
                $the_query = new WP_Query(array(
                    'cat' => $myCat,
                    'posts_per_page' => 4,
                    'offset' => 8
                ));
                while ($the_query->have_posts()) :
                    $the_query->the_post();
                    ?>
                    <li>
                        <div class="post-thumbnail bkg-ltgrey"><?php the_post_thumbnail('medium', array('class' => 'alignleft')); ?></div>
                        <a href="<?php the_permalink(); ?>" style="line-height:21px; padding:0; display:block; background-color:transparent;"><br>
                    <?php the_title(); ?></a>       
                    </li>
                    <?php
                endwhile;
                wp_reset_postdata();
                ?>   </div><!-- item -->

        </div><!-- carousel-inner -->
        <a class="left carousel-control" href="#myCarousel-<?php echo $myCat ?>" data-slide="prev"></a>
        <a class="right carousel-control" href="#myCarousel-<?php echo $myCat ?>" data-slide="next"></a>
    </div><!-- #myCarousel -->      

</ul>