<!-- THESE SNIPPETS WILL NEED TO BE CONVERTED INTO SHORTCODES. YOU MAY NEED TO SEEK CLARITY FROM THE TEAM LEADERS TO DETERMINE HOW THESE ARE CUSTOMIZED WITHIN EACH POST -->

<!-- SHORTCODE #1 -- SUBSCRIBE CTA FULL WIDTH BAND -- MIGHT BE A STANDARD ELEMENT WITHOUT MUCH NEED OF CUSTOMIZATION OF TEXT OR COLOR -->
<div class="band bkg-blue">
<div class="container">
<div class="row">

<div class="col-md-1"></div>
<div class="col-md-6"><h6>Get access to industry-leading social content</h6></div>

<div class="col-md-4 subscribe">
<form><input type="email" placeholder="your email"><button class="bkg-yellow">Subscribe</button></form>
</div><!-- /.subscribe -->

<div class="col-md-1"></div>

</div>
</div>
</div><!-- /.band -->


<!-- SHORTCODE #2 -- INSET GREY CALLOUT -- NOT SURE HOW THIS ONE NEEDS TO BE CUSTOMIZED -->
<div class="callout bkg-grey col-md-6">
<img src="<?php echo esc_url(get_stylesheet_directory_uri());?>/assets//images/guide.png" class="fleft">
<h4>the survival guide to customer experience</h4>
<p>20 CX experts show us how to manage customer experiences across all touchpoints</p>
<button class="bkg-blue">get the free guide</button>
</div>



<!-- SHORTCODE #3 -- RECOMMENDED CONTENT FULL-WIDTH BAND -- THESE WILL LIKELY BE DRAWN FROM RELATED POSTS WITHIN THE SAME CATEGORY. I DO NOT KNOW HOW THE TEAM WANTS THESE RELATIONSHIPS TO WORK. MAY REQUIRE SOME LOOP CUSTOMIZATION -->
<div class="band bkg-grey">
<div class="container">
<div class="row">

<div class="col-md-1"></div>
<div class="col-md-10 callout">
<img src="<?php echo esc_url(get_stylesheet_directory_uri());?>/assets//images/guide.png" class="fleft">
<p><em>recommended by Sprinklr</em></p>
<h4>the survival guide to customer experience</h4>
<p>20 CX experts show us how to manage customer experiences across all touchpoints</p>
<button class="bkg-blue">get the free guide</button>
</div>
<div class="col-md-1"></div>

</div>
</div>
</div><!-- /.band -->