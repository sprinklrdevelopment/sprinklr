<!-- THE HEADER AND FOOTER MENUS ARE JUST PLACEHOLDER IMAGES. AS YOU WILL BE USING THE SAME MENUS FOR THE REST OF THE SITE, YOU CAN SIMPLY PASTE YOUR CODE HERE -->
<header>
<div class="container">
<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
<img src="<?php echo esc_url(get_stylesheet_directory_uri());?>/assets/images/SprinklrLogo.png" alt="header" class="fleft" style="margin:20px 0 0px 0; width:100px; height:auto;">
<img src="<?php echo esc_url(get_stylesheet_directory_uri());?>/assets/images/navbar.png" alt="header" class="fright">
</div>
<div class="col-md-1"></div>
</div>
</div>
</header>