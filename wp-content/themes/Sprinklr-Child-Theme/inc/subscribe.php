<div class="band bkg-green">
<div class="container">
<div class="row">

<div class="col-md-1"></div>
<h6 class="col-md-6"><?php esc_html_e('Insights, ideas &amp; Stories to help brands create experiences that customers love.', 'Sprinklr');?></h6>

<div class="col-md-4 subscribe">
<form><input type="email" placeholder="<?php esc_html_e('Enter Your Email', 'Sprinklr');?>"><button class="bkg-yellow"><?php esc_html_e('Subscribe', 'Sprinklr');?></button></form>
</div><!-- /.subscribe -->

<div class="col-md-1"></div>

</div>
</div>
</div><!-- /.band -->