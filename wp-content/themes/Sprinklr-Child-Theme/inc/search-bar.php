<div class="col-md-1 fright"></div>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $(document).on('click', '.dropdown-menu', function (e) {
            $(this).hasClass('keep_open') && e.stopPropagation(); // This replace if conditional.
        });
    });
</script>​

<div class="search-box fright" id="Search">
    <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img src="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/assets/images/search.png" title="<?php esc_attr_e('Search', 'Sprinklr'); ?>">
        </button>
        <div class="dropdown-menu keep_open" >      <form method="get" class="form-search search" id="searchform" action="<?php echo esc_url(home_url('/')); ?>">
                <input type="text" placeholder="<?php esc_attr_e('search', 'Sprinklr'); ?>" name="s" id="s" ><button class="bkg-blue" type="submit" id="searchsubmit"><?php esc_attr_e('search', 'Sprinklr'); ?></button></form></div>


    </div>
</div>


</div>