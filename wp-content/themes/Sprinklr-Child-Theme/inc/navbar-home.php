<div class="navbar" >
    <div class="col-md-1"></div>
    <div class="col-md-3">
        <form class="subscribe">
            <input type="email" placeholder="<?php esc_html_e('Enter Your Email', 'Sprinklr');?>"><button class="bkg-blue"><?php esc_attr_e('subscribe', 'Sprinklr');?></button>
        </form>
    </div><!-- /.subscribe -->
    <div class="col-md-6">      
        <nav class="navbar-collapse collapse nav-home">    
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#All"><?php esc_attr_e('All', 'Sprinklr');?></a>
                    <?php
                    $args = array(
                        'hide_empty' => 1,
                        'orderby' => 'name',
                        'order' => 'ASC',
                    );
                    $categories = get_categories($args);
                    foreach ($categories as $category) {
                        if (cat_is_ancestor_of(4, $category)) {

                            echo
                            '<li>
                                <a href="#' . $category->slug . '" role="tab" data-toggle="tab">    
                                    ' . $category->name . '
                                </a>
                            </li>';
                        }
                       
                    }
                    echo '</ul>';
                    ?>
                    </nav>
                    </div>


<?php include_once get_stylesheet_directory() . '/inc/search-bar.php'; ?>
<?php wp_reset_postdata(); ?>